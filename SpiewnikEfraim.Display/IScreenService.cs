﻿using System.ServiceModel;
using SpiewnikEfraim.Logic.Dto;

namespace SpiewnikEfraim.Display
{
    [ServiceContract]
    public interface IScreenService
    {
        [OperationContract]
        int GetMonitorsCount();

        [OperationContract]
        bool SetScreenNonPrimaryMonitorFirst();

        [OperationContract]
        bool SetScreenPrimaryMonitorFirst();

        [OperationContract]
        ScreenData[] GetMonitorsProperties();

        [OperationContract]
        ScreenData GetMonitorProperties(int monitorNumber);

        [OperationContract]
        void SetScreenContent(SlideData screenContent);

        [OperationContract]
        SlideData GetCurrentSlideContent();

        [OperationContract]
        bool ServiceAvailable();

        [OperationContract]
        void ShutdownServer();

        [OperationContract]
        void RestartServer();
    }
}
