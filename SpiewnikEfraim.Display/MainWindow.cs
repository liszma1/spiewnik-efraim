﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ServiceModel;
using SpiewnikEfraim.Logic.Helpers;
using System.Diagnostics;
using CefSharp;
using CefSharp.WinForms;
using Microsoft.Win32;
using SpiewnikEfraim.Logic.Dto;

namespace SpiewnikEfraim.Display
{
    public partial class MainWindow : Form
    {
        private ServiceHost _host;
        private ChromiumWebBrowser _chromeBrowser;
        private const string EmptySlide = @"<html><body style=""background-color: black; overflow: hidden;""></body></html>";
        private SlideData _screenContent = new SlideData
        {
            PageText = EmptySlide,
            SongId = -1,
            Id = -1
        };

        public MainWindow()
        {
            InitializeComponent();
        }

        public void SetProvidedContent(SlideData screenContent)
        {
            _screenContent = screenContent;
            Invoke((MethodInvoker)(() => _chromeBrowser.LoadString(_screenContent.PageText, "http://spiewnikEfraim")));
        }

        public SlideData GetCurrentSlideContent()
        {
            return _screenContent;
        }

        public void SetWindowPosition(int x, int y)
        {
            Location = new Point(x, y);
          
            BringToFront();
            Activate();
        }

        public void ShutdownServer()
        {
            Process.Start("shutdown", "/p /f");
        }

        public void RestartServer()
        {
            Process.Start("shutdown", "/f /r /t 0");
        }

        private void HandleScreenResolutionChanges()
        {
            SystemEvents.DisplaySettingsChanged += SystemEvents_DisplaySettingsChanged;
        }

        private void ReleaseScreenResolutionChangesHandler()
        {
            SystemEvents.DisplaySettingsChanged -= SystemEvents_DisplaySettingsChanged;
        }

        private void SystemEvents_DisplaySettingsChanged(object sender, EventArgs e)
        {
            var screenHelper = new ScreenHelper();
            var screenToProcess = screenHelper.GetPrimaryMonitorProperties();

            if (screenToProcess == null)
                return;

            SetWindowPosition(screenToProcess.Bounds.X, screenToProcess.Bounds.Y);
        }

        private void PrepareControls()
        {
            Dock = DockStyle.Fill;
            Anchor = AnchorStyles.Top | AnchorStyles.Left;
        }
        
        private void MainWindow_Load(object sender, EventArgs e)
        {
            InitChromium();
            HandleScreenResolutionChanges();

            var screenBounds = Screen.FromControl(this).Bounds;
            SetWindowPosition(screenBounds.X, screenBounds.Y);

            _host = new ServiceHost(new ScreenService(this));
            _host.Open();

            var screenHelper = new ScreenHelper();
            var screenToProcess = screenHelper.GetPrimaryMonitorProperties();

            if (screenToProcess == null)
                return;

            TaskbarHelper.Hide();
            Cursor.Hide();
            PrepareControls();
            SetWindowPosition(screenToProcess.Bounds.X, screenToProcess.Bounds.Y);
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_host != null 
                && _host.State != CommunicationState.Faulted
                && _host.State != CommunicationState.Closed
                && _host.State != CommunicationState.Closing)
                _host.Close();

            Cef.Shutdown();
            TaskbarHelper.Show();
            Cursor.Show();
            ReleaseScreenResolutionChangesHandler();
        }

        private void InitChromium()
        {
            var cefSettings = new CefSettings()
            {
                BackgroundColor = Cef.ColorSetARGB(0, 0, 0, 0)
            };
            Cef.Initialize(cefSettings);

            _chromeBrowser = new ChromiumWebBrowser("http://rendering")
            {
                Dock = DockStyle.Fill,
                Location = new Point(0, 0)
            };
            _chromeBrowser.LoadHtml(EmptySlide);
           
            Controls.Add(_chromeBrowser);
        }
    }
}
