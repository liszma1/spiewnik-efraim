﻿using System.Linq;
using System.ServiceModel;
using SpiewnikEfraim.Logic.Helpers;
using System.Windows.Forms;
using SpiewnikEfraim.Logic.Dto;

namespace SpiewnikEfraim.Display
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ScreenService : IScreenService
    {
        private readonly ScreenHelper _screenHelper;
        private readonly MainWindow _mainWindow;

        public ScreenService(MainWindow mainForm) : this()
        {
            _mainWindow = mainForm;
        }

        public ScreenService()
        {
            _screenHelper = new ScreenHelper();
        }

        public int GetMonitorsCount()
        {
            return _screenHelper.GetMonitorsCount();
        }

        public ScreenData[] GetMonitorsProperties()
        {
            return _screenHelper.GetAllScreens().Select(elem => new ScreenData
            {
                BitsPerPixel = 32,
                Bounds = new RectangleData
                {
                    Bottom = elem.Bounds.Bottom,
                    Height = elem.Bounds.Height,
                    Left = elem.Bounds.Left,
                    Right = elem.Bounds.Right,
                    Top = elem.Bounds.Top,
                    Width = elem.Bounds.Width,
                    X = elem.Bounds.X,
                    Y = elem.Bounds.Y
                },
                DeviceName = elem.DeviceName,
                Primary = elem.IsPrimary
            }).ToArray();
        }

        public ScreenData GetMonitorProperties(int monitorNumber)
        {
            var monitorProperty = _screenHelper.GetMonitorProperties(monitorNumber);

            if (monitorProperty == null)
                return null;

            return new ScreenData
            {
                BitsPerPixel = 32,
                Bounds = new RectangleData
                {
                    Bottom = monitorProperty.Bounds.Bottom,
                    Height = monitorProperty.Bounds.Height,
                    Left = monitorProperty.Bounds.Left,
                    Right = monitorProperty.Bounds.Right,
                    Top = monitorProperty.Bounds.Top,
                    Width = monitorProperty.Bounds.Width,
                    X = monitorProperty.Bounds.X,
                    Y = monitorProperty.Bounds.Y
                },
                DeviceName = monitorProperty.DeviceName,
                Primary = monitorProperty.IsPrimary
            };
        }

        public void SetScreenContent(SlideData screenContent)
        {
            _mainWindow.SetProvidedContent(screenContent);
        }

        public bool SetScreenNonPrimaryMonitorFirst()
        {
            var screenToProcess = 
                _screenHelper.GetNonPrimaryMonitorProperties() ?? 
                _screenHelper.GetPrimaryMonitorProperties();

            if (screenToProcess == null)
                return false;

            _mainWindow.Invoke((MethodInvoker) (() => _mainWindow.SetWindowPosition(screenToProcess.Bounds.X, screenToProcess.Bounds.Y) ));
            return true;
        }

        public bool SetScreenPrimaryMonitorFirst()
        {
            var screenToProcess = _screenHelper.GetPrimaryMonitorProperties();

            if (screenToProcess == null)
                return false;

            _mainWindow.Invoke((MethodInvoker)(() => _mainWindow.SetWindowPosition(screenToProcess.Bounds.X, screenToProcess.Bounds.Y)));
            return true;
        }

        public SlideData GetCurrentSlideContent()
        {
            return _mainWindow.GetCurrentSlideContent();
        }

        public bool ServiceAvailable()
        {
            return true;
        }

        public void ShutdownServer()
        {
            _mainWindow.ShutdownServer();
        }

        public void RestartServer()
        {
            _mainWindow.RestartServer();
        }
    }
}
