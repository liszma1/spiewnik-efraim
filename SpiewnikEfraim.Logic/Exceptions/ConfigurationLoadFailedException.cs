﻿using System;

namespace SpiewnikEfraim.Web.Exceptions
{
    public class ConfigurationLoadFailedException : Exception
    {
        public ConfigurationLoadFailedException() : base("Nie udało się zainicjalizować konfiguracji")
        {
        }
    }
}