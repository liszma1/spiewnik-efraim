﻿using SpiewnikEfraim.Logic.Dto;

namespace SpiewnikEfraim.Logic.Helpers
{
    public interface ITvHelper
    {
        bool ExecuteAction(TvActionDto actionToExecute);
    }
}
