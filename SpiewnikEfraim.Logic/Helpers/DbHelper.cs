﻿using System;
using System.Collections.Generic;
using System.Linq;
using SpiewnikEfraim.Logic.Dto;
using LiteDB;
using System.IO;
using System.IO.Compression;
using Newtonsoft.Json;
using SpiewnikEfraim.Logic.Services;

namespace SpiewnikEfraim.Logic.Helpers
{
    public class DbHelper : IDbHelper, IDisposable
    {
        private readonly LiteDatabase _databaseObject;
        private IConfigProvider ConfigProvider { get; }

        public DbHelper(IConfigProvider configProvider)
        {
            ConfigProvider = configProvider ?? throw new ArgumentNullException(nameof(configProvider));

            if (string.IsNullOrEmpty(ConfigProvider.DatabasePath))
                throw new ArgumentNullException(ConfigProvider.DatabasePath);

            if (!File.Exists(ConfigProvider.DatabasePath))
                throw new InvalidOperationException(ConfigProvider.DatabasePath + " nie istnieje");

            _databaseObject = new LiteDatabase(ConfigProvider.DatabasePath);
        }

        public SlideData[] SearchInSlides(string textToFind, bool skipResultsFromSameSong)
        {
            if (string.IsNullOrWhiteSpace(textToFind))
                return new SlideData[0];

            var slidesCollection = _databaseObject.GetCollection<SlideData>("songPages");
            var pagesToReturn = slidesCollection.Find(x => x.SearchPageText.Contains(textToFind)).ToArray();

            if (skipResultsFromSameSong)
                return pagesToReturn
                    .OrderBy(x => x.Id)
                    .GroupBy(y => y.SongId)
                    .Select(z => z.First())
                    .ToArray();

            return pagesToReturn.ToArray();
        }

        public SlideData GetSlideDataBySongName(string songName)
        {
            var songsCollection = _databaseObject.GetCollection<SongData>("songs");
            var songPagesCollection = _databaseObject.GetCollection<SlideData>("songPages");
            var songObject = songsCollection.FindOne(x => x.Name == songName);

            var result = songObject == null ? null : songPagesCollection.FindOne(x => x.SongId == songObject.Id);
            
            return result;
        }

        public SlideData[] GetSongSlidesById(int songId)
        {
            var songsCollection = _databaseObject.GetCollection<SongData>("songs");
            var songPagesCollection = _databaseObject.GetCollection<SlideData>("songPages");
            var songObject = songsCollection.FindOne(x => x.Id == songId);

            return songObject == null ? null : songPagesCollection.Find(x => x.SongId == songObject.Id).OrderBy(y => y.Order).ToArray();
        }

        public SlideData GetSlideData(int slideId)
        {
            var slidesCollection = _databaseObject.GetCollection<SlideData>("songPages");
            return slidesCollection.FindById(slideId);
        }

        public SongData GetSongDataById(int songId)
        {
            return _databaseObject.GetCollection<SongData>("songs").FindOne(x => x.Id == songId);
        }

        public SongData GetSongDataByName(string songName)
        {
            return _databaseObject.GetCollection<SongData>("songs").FindOne(x => x.Name == songName);
        }

        public SongData[] GetSpecialSongs()
        {
            return _databaseObject.GetCollection<SongData>("songs").Find(x => x.Protected).ToArray();
        }

        public SongData[] GetAllSongs()
        {
            return _databaseObject.GetCollection<SongData>("songs").FindAll().ToArray();
        }

        public SlideData[] GetAllSlides()
        {
            return _databaseObject.GetCollection<SlideData>("songPages").FindAll().ToArray();
        }

        public bool RemoveSong(int songId)
        {
            var songsCollection = _databaseObject.GetCollection<SongData>("songs");
            var songPagesCollection = _databaseObject.GetCollection<SlideData>("songPages");

            var songObject = songsCollection.FindById(songId);

            if (songObject == null)
                return false;

            if (songObject.Protected)
                return false;

            songPagesCollection.Delete(x => x.SongId == songObject.Id);
            return songsCollection.Delete(songObject.Id);
        }

        public int SaveSong(SongFromEditor songFromEditor)
        {
            var songsCollection = _databaseObject.GetCollection<SongData>("songs");
            var songPagesCollection = _databaseObject.GetCollection<SlideData>("songPages");

            SongData songObject;
            var isSongNew = songFromEditor.SongData.Id <= 0;

            if (songFromEditor.SongData.Id > 0)
                songObject = songsCollection.FindById(songFromEditor.SongData.Id);
            else
            {
                songObject = new SongData
                {
                    Created = DateTime.Now,
                    Id = 0
                };
            }

            songObject.Indexable = songFromEditor.SongData.Indexable;
            songObject.Modified = songFromEditor.SongData.Modified;
            songObject.Name = songFromEditor.SongData.Name;
            songObject.NameNormalized = SearchHelper.NormalizeText(songFromEditor.SongData.Name);
            songObject.PaperSongBookId = songFromEditor.SongData.PaperSongBookId ?? string.Empty;

            var songUpdateResult = true;

            if (isSongNew)
                songObject.Id = songsCollection.Insert(songObject);
            else
                songUpdateResult = songsCollection.Update(songObject);

            if (!songUpdateResult || songObject.Id <= 0)
                return -1;

            if (isSongNew)
                songPagesCollection.Insert(songFromEditor.SlidesData.Select(x =>
                {
                    var slideData = new SlideData
                    {
                        Id = 0,
                        Created = x.Created,
                        Modified = x.Modified,
                        SongId = songObject.Id,
                        Order = x.Order,
                        PageText = x.PageText,
                        CustomHtml = x.CustomHtml,
                        SearchPageText = string.Empty
                    };

                    if (!songObject.Indexable)
                        return slideData;

                    slideData.SearchPageText = slideData.Order == 1
                        ? $"{SearchHelper.NormalizeText(songObject.PaperSongBookId)} {songObject.NameNormalized} {SearchHelper.NormalizeText(slideData.PageText ?? string.Empty)}".Trim()
                        : SearchHelper.NormalizeText(slideData.PageText ?? string.Empty);

                    return slideData;
                }));
            else
            {
                var currentSlidesInDatabase = songPagesCollection.Find(x => x.SongId == songObject.Id).ToArray();
                var newSlidesIds = songFromEditor.SlidesData.Select(y => y.Id).ToArray();
                
                foreach (var slide in currentSlidesInDatabase)
                {
                    // Delete not existing anymore slides differential
                    if (!newSlidesIds.Contains(slide.Id))
                        songPagesCollection.Delete(slide.Id);
                    else
                    {
                        // Update existing slides
                        var newSlideObject = songFromEditor.SlidesData.FirstOrDefault(x => x.Id == slide.Id);

                        if (newSlideObject == null)
                            continue;

                        slide.CustomHtml = newSlideObject.CustomHtml;
                        slide.Modified = newSlideObject.Modified;
                        slide.Order = newSlideObject.Order;
                        slide.PageText = newSlideObject.PageText;

                        if (songObject.Indexable)
                        {
                            slide.SearchPageText = newSlideObject.Order == 1
                                ? $"{SearchHelper.NormalizeText(songObject.PaperSongBookId)} {songObject.NameNormalized} {SearchHelper.NormalizeText(newSlideObject.PageText ?? string.Empty)}".Trim()
                                : SearchHelper.NormalizeText(newSlideObject.PageText ?? string.Empty);
                        }
                        else
                            slide.SearchPageText = string.Empty;

                        songPagesCollection.Update(slide);
                    }
                }

                // Add new slides
                songPagesCollection.Insert(songFromEditor.SlidesData.Where(x => x.Id <= 0).Select(y =>
                {
                    var slideData = new SlideData
                    {
                        Created = y.Created,
                        CustomHtml = y.CustomHtml,
                        Id = 0,
                        SongId = songObject.Id,
                        PageText = y.PageText,
                        Modified = y.Modified,
                        SearchPageText = string.Empty,
                        Order = y.Order
                    };

                    if (!songObject.Indexable)
                        return slideData;

                    slideData.SearchPageText = slideData.Order == 1
                        ? $"{SearchHelper.NormalizeText(songObject.PaperSongBookId)} {songObject.NameNormalized} {SearchHelper.NormalizeText(slideData.PageText ?? string.Empty)}".Trim()
                        : SearchHelper.NormalizeText(slideData.PageText ?? string.Empty);

                    return slideData;
                }));
            }

            return songObject.Id;
        }

        public bool RebuildDatabase()
        {
            var songsCollection = _databaseObject.GetCollection<SongData>("songs");
            var songPagesCollection = _databaseObject.GetCollection<SlideData>("songPages");

            var songsList = new List<SongData>(songsCollection.FindAll());
            var songPagesList = new List<SlideData>(songPagesCollection.FindAll());

            if (!_databaseObject.DropCollection(songsCollection.Name))
                return false;

            if (!_databaseObject.DropCollection(songPagesCollection.Name))
                return false;

            songsCollection = _databaseObject.GetCollection<SongData>("songs");
            songPagesCollection = _databaseObject.GetCollection<SlideData>("songPages");

            foreach (var song in songsList)
            {
                var songSlides = songPagesList.Where(x => x.SongId == song.Id).ToArray();

                song.Id = 0;
                song.NameNormalized = SearchHelper.NormalizeText(song.Name);
                var songId = songsCollection.Insert(song);

                foreach (var slide in songSlides)
                {
                    slide.Id = 0;
                    slide.SongId = songId;
                    slide.SearchPageText = string.Empty;

                    if (slide.Order == 1 && song.Indexable)
                        slide.SearchPageText = $"{SearchHelper.NormalizeText(song.PaperSongBookId)} {song.NameNormalized} {SearchHelper.NormalizeText(slide.PageText ?? string.Empty)}".Trim();
                    else if (song.Indexable)
                        slide.SearchPageText = $"{SearchHelper.NormalizeText(slide.PageText ?? string.Empty)}".Trim();
                }

                songPagesCollection.Insert(songSlides);
            }

            songsCollection.EnsureIndex(x => x.Id, true);
            songsCollection.EnsureIndex(x => x.Name);
            songPagesCollection.EnsureIndex(x => x.Id, true);
            songPagesCollection.EnsureIndex(x => x.SongId);

            return true;
        }

        public string[] ImportSongFromJson(string jsonString, bool forceSave)
        {
            var jsonSongs = JsonConvert.DeserializeObject<JsonSong[]>(jsonString);
            var result = new List<string>();

            foreach (var song in jsonSongs)
            {
                var existingSongData = GetSongDataByName(song.Name);

                if (existingSongData == null)
                {
                    var songId = SaveSong(new SongFromEditor
                    {
                        SongData = new SongData
                        {
                            Name = song.Name,
                            Created = song.Created,
                            Indexable = song.Indexable,
                            Modified = song.Modified,
                            PaperSongBookId = song.PaperSongBookId
                        },
                        SlidesData = song.Slides.Select(x => new SlideData
                        {
                            Created = x.Created,
                            Modified = x.Modified,
                            CustomHtml = x.CustomHtml,
                            Order = x.Order,
                            PageText = x.PageText
                        }).ToArray()
                    });

                    result.Add(songId <= 0
                        ? $"Zapis pieśni: {song.Name} nie powiódł się"
                        : $"Zapisano pieśń o nazwie: {song.Name}");
                }
                else
                {
                    if (!forceSave)
                        result.Add($"Istnieje już pieśń o nazwie: {song.Name}");
                    else
                    {
                        var fileCounter = 2;

                        while (GetSongDataByName($"{song.Name}({fileCounter})") != null)
                            fileCounter++;
                        
                        var songId = SaveSong(new SongFromEditor
                        {
                            SongData = new SongData
                            {
                                Name = $"{song.Name}({fileCounter})",
                                Created = song.Created,
                                Indexable = song.Indexable,
                                Modified = song.Modified,
                                PaperSongBookId = song.PaperSongBookId
                            },
                            SlidesData = song.Slides.Select(x => new SlideData
                            {
                                Created = x.Created,
                                Modified = x.Modified,
                                CustomHtml = x.CustomHtml,
                                Order = x.Order,
                                PageText = x.PageText
                            }).ToArray()
                        });

                        result.Add(songId <= 0
                            ? $"Zapis pieśni: {song.Name}({fileCounter}) nie powiódł się"
                            : $"Zapisano pieśń o nazwie: {song.Name}({fileCounter})");
                    }
                }
            }

            return result.ToArray();
        }

        public MemoryStream ExportSongAsZippedHtml(int songId)
        {
            var songData = GetSongDataById(songId);
            var songSlides = GetSongSlidesById(songId);

            if (songSlides == null)
                return null;

            var memoryStream = new MemoryStream();
            
            using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
            {
                foreach (var slide in songSlides)
                {
                    var htmlFile = archive.CreateEntry($"{songData.NameNormalized}_{slide.Order}.html");

                    using (var entryStream = htmlFile.Open())
                    {
                        using (var streamWriter = new StreamWriter(entryStream))
                        {
                            streamWriter.Write(slide.CustomHtml ?
                                slide.PageText :
                                string.Format(ConfigProvider.SlideTemplate, slide.PageText));
                        }
                    }
                }
            }

            memoryStream.Position = 0;

            return memoryStream;
        }

        public JsonSong ExportSongAsJson(int songId)
        {
            var songData = GetSongDataById(songId);
            var songSlides = GetSongSlidesById(songId);

            if (songSlides == null)
                return null;
            
            return new JsonSong
            {
                Name = songData.Name,
                Indexable = songData.Indexable,
                Modified = songData.Modified,
                Created = songData.Created,
                PaperSongBookId = songData.PaperSongBookId,
                Slides = songSlides.Select(x => new JsonSlide
                {
                    PageText = x.PageText,
                    Order = x.Order,
                    Modified = x.Modified,
                    Created = x.Created,
                    CustomHtml = x.CustomHtml
                }).ToArray()
            };
        }

        public MemoryStream ExportSongsAsZippedHtml()
        {
            var songs = GetAllSongs();
            var songSlides = GetAllSlides();

            var memoryStream = new MemoryStream();

            using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
            {
                foreach (var song in songs)
                {
                    var slides = songSlides.Where(x => x.SongId == song.Id); 

                    foreach (var slide in slides)
                    {
                        var htmlFile = archive.CreateEntry($"{song.NameNormalized}_{slide.Order}.html");

                        using (var entryStream = htmlFile.Open())
                        {
                            using (var streamWriter = new StreamWriter(entryStream))
                            {
                                streamWriter.Write(slide.CustomHtml
                                    ? slide.PageText
                                    : string.Format(ConfigProvider.SlideTemplate, slide.PageText));
                            }
                        }
                    }
                }
            }

            memoryStream.Position = 0;

            return memoryStream;
        }

        public JsonSong[] ExportSongsAsJson()
        {
            var songData = GetAllSongs();
            var songSlides = GetAllSlides();

            if (songSlides == null)
                return null;

            return songData.Select(song => new JsonSong
            {
                Name = song.Name,
                Indexable = song.Indexable,
                Modified = song.Modified,
                Created = song.Created,
                PaperSongBookId = song.PaperSongBookId,
                Slides = songSlides.Where(slides => slides.SongId == song.Id).Select(slide => new JsonSlide
                {
                    PageText = slide.PageText,
                    Order = slide.Order,
                    Modified = slide.Modified,
                    Created = slide.Created,
                    CustomHtml = slide.CustomHtml
                }).ToArray()
            }).ToArray();
        }

        public void Dispose()
        {
            _databaseObject?.Dispose();
        }
    }
}
