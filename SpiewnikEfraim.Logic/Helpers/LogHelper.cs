﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SpiewnikEfraim.Logic.Helpers
{
    public enum LoggerName
    {
        MainWindow
    }

    public class LogHelper
    {
        private readonly LoggerName _loggerName;
        private readonly Dictionary<DateTime, string> _logList;

        public LogHelper(LoggerName loggerName)
        {
            _loggerName = loggerName;
            _logList = new Dictionary<DateTime, string>();
        }

        public void Log(string message)
        {
            _logList.Add(DateTime.Now, message);
        }

        public void Fatal(string message)
        {
            var currentDate = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");
            using (var file = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + $"log_fatal_{_loggerName.ToString()}_{currentDate}.txt"))
            {
                file.WriteLine("[{0}] {1}", DateTime.Now, message);
            }
        }

        public void Flush()
        {
            var currentDate = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");
            using (var file = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + $"log_{_loggerName.ToString()}_{currentDate}.txt"))
            {
                foreach (var entry in _logList)
                    file.WriteLine("[{0}] {1}", entry.Key, entry.Value);
            }

            _logList.Clear();
        }
    }
}
