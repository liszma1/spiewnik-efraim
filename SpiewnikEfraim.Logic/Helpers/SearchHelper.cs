﻿using System;
using System.Collections.Generic;

namespace SpiewnikEfraim.Logic.Helpers
{
    public static class SearchHelper
    {
        private static readonly char[] CharactersToRemove;
        private static readonly Dictionary<string, string> CharactersToReplace;

        static SearchHelper()
        {
            CharactersToRemove = new char[]
            {
                '!', '@', '#', '$', '%', '^', '&',
                '*', '(', ')', '-', '_', '+', '=',
                '{', '[', '}', ']', ':', ';', '"',
                '\'', '|', '\\', '<', ',', '>', '.',
                '?', '/', '…'
            };
            CharactersToReplace = new Dictionary<string, string>()
            {
                { "ę", "e" },
                { "ó", "o" },
                { "ą", "a" },
                { "ś", "s" },
                { "ł", "l" },
                { "ż", "z" },
                { "ź", "z" },
                { "ć", "c" },
                { "ń", "n" }
            };
        }

        private static string ReplaceCharacters(string inputString)
        {
            foreach (var character in CharactersToReplace)
                inputString = inputString.Replace(character.Key, character.Value);

            return inputString;
        }

        public static string NormalizeText(string inputString)
        {
            if (string.IsNullOrWhiteSpace(inputString))
                return inputString;

            inputString = inputString
                .ToLower()
                .Replace("<br/>", " ")
                .Replace("<br>", " ")
                .Replace("</p>", " ")
                .Replace("<p>", " ")
                .Replace(@"<p class=""slideparagraph"">", " ");

            var tempString = inputString.Split(CharactersToRemove, StringSplitOptions.RemoveEmptyEntries);
            return ReplaceCharacters(string.Join(" ", tempString).Replace("–", " ").Replace("\n", " ").Replace("   ", " ").Replace("  ", " "));
        }

        public static string PrepareSongTextToDatabase(string inputString)
        {
            if (string.IsNullOrWhiteSpace(inputString))
                return inputString;

            var splittedText = new List<string>(inputString.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries));

            for (var i = 0; i < splittedText.Count; i++)
            {
                splittedText[i] = splittedText[i].Trim();

                if (int.TryParse(splittedText[i], out _))
                {
                    splittedText.RemoveAt(i);
                    i--;
                }
            }

            return @"<p class=""slideParagraph"">" + string.Join("<br/>", splittedText) + "</p>";
        }
    }
}
