﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using SpiewnikEfraim.Logic.Dto;
using SpiewnikEfraim.Logic.Services;

namespace SpiewnikEfraim.Logic.Helpers
{
    public class TvHelper : ITvHelper
    {
        private readonly KeyValuePair<TvActionDto, string>[] _tvActions = {
            new KeyValuePair<TvActionDto, string>(TvActionDto.TurnOff, "POWR0   "),
            new KeyValuePair<TvActionDto, string>(TvActionDto.TurnOn, "POWR1   "),
            new KeyValuePair<TvActionDto, string>(TvActionDto.SwitchToHdmi1, "IAVD4   "),
            new KeyValuePair<TvActionDto, string>(TvActionDto.SwitchToHdmi2, "IAVD5   "),
            new KeyValuePair<TvActionDto, string>(TvActionDto.SwitchToHdmi3, "IAVD6   ")
        };

        private readonly IPAddress _tvAddress;
        private readonly int _tvPort;

        public TvHelper(IConfigProvider configProvider)
        {
            if (configProvider == null)
                throw new ArgumentNullException(nameof(configProvider));

            _tvAddress = configProvider.TvAddress;
            _tvPort = configProvider.TvPort;
        }

        public bool ExecuteAction(TvActionDto actionToExecute)
        {
            if (_tvActions.All(x => x.Key != actionToExecute))
                throw new ArgumentOutOfRangeException(nameof(actionToExecute));

            var commandToExecute = _tvActions.First(x => x.Key == actionToExecute).Value + '\r';

            if (string.IsNullOrEmpty(commandToExecute))
                throw new ArgumentNullException(commandToExecute);

            var tvEndPoint = new IPEndPoint(_tvAddress, _tvPort);
            var tvServer = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            tvServer.Connect(tvEndPoint);
            tvServer.Send(Encoding.ASCII.GetBytes(commandToExecute));

            var receivedData = new byte[1024];
            var receivedDataLength = tvServer.Receive(receivedData);
            var stringData = Encoding.ASCII.GetString(receivedData, 0, receivedDataLength);
            
            tvServer.Shutdown(SocketShutdown.Both);
            tvServer.Close();

            return stringData == "OK";
        }
    }
}
