﻿using System;
using Microsoft.Win32;
using SpiewnikEfraim.Logic.Dto;

namespace SpiewnikEfraim.Logic.Helpers
{
    public static class ComponentHelper
    {
        public static ErrorStatus SetIe11KeyForHtmlPreview(string appName, out string errorMessage)
        {
            errorMessage = string.Empty;

            RegistryKey regkey = null;
            try
            {
                regkey = Registry.LocalMachine.OpenSubKey(Environment.Is64BitOperatingSystem ?
                    @"SOFTWARE\\Wow6432Node\\Microsoft\\Internet Explorer\\MAIN\\FeatureControl\\FEATURE_BROWSER_EMULATION" :
                    @"SOFTWARE\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION", true);

                if (regkey == null)
                    return ErrorStatus.RegistryKeyNotFound;
                
                var findAppkey = Convert.ToString(regkey.GetValue(appName));

                if (findAppkey == "11001")
                    return ErrorStatus.Ok;

                if (string.IsNullOrEmpty(findAppkey))
                    regkey.SetValue(appName, 0x2AF9, RegistryValueKind.DWord);

                findAppkey = Convert.ToString(regkey.GetValue(appName));

                if (findAppkey == "11001")
                    return ErrorStatus.Ok;

                return ErrorStatus.RegistryKeySaveFailed;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return ErrorStatus.UndefinedError;
            }
            finally
            {
                regkey?.Close();
            }
        }
    }
}
