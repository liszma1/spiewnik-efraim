﻿
using System.IO;
using SpiewnikEfraim.Logic.Dto;

namespace SpiewnikEfraim.Logic.Helpers
{
    public interface IDbHelper
    {
        SongData GetSongDataById(int songId);
        SlideData[] SearchInSlides(string textToFind, bool skipResultsFromSameSong);
        SlideData GetSlideData(int slideId);
        SlideData GetSlideDataBySongName(string songName);
        SlideData[] GetSongSlidesById(int songId);
        SongData[] GetAllSongs();
        SlideData[] GetAllSlides();
        bool RemoveSong(int songId);
        int SaveSong(SongFromEditor songFromEditor);
        SongData[] GetSpecialSongs();
        bool RebuildDatabase();
        MemoryStream ExportSongAsZippedHtml(int songId);
        MemoryStream ExportSongsAsZippedHtml();
        JsonSong ExportSongAsJson(int songId);
        JsonSong[] ExportSongsAsJson();
        string[] ImportSongFromJson(string jsonString, bool forceSave);
    }
}
