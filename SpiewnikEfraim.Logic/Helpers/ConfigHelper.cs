﻿using SpiewnikEfraim.Logic.Services;

namespace SpiewnikEfraim.Logic.Helpers
{
    public class ConfigHelper
    {
        private readonly string _databasePath;

        public ConfigHelper(IConfigProvider configProvider)
        {
            _databasePath = configProvider.DatabasePath;
        }

        public string GetDatabaseFilePath()
        {
            return _databasePath;
        }
    }
}