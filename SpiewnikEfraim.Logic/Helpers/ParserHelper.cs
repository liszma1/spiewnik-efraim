﻿using System.Collections.Generic;
using System.Linq;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.IO;
using SpiewnikEfraim.Logic.Dto;
using LiteDB;

namespace SpiewnikEfraim.Logic.Helpers
{
    public class ParserHelper
    {
        private readonly Dictionary<int, SlideData> _parsedPdf;
        private readonly Dictionary<int, int> _songEdges;

        public ParserHelper(string pdfFilePath)
        {
            if (string.IsNullOrWhiteSpace(pdfFilePath))
                throw new ArgumentNullException(nameof(pdfFilePath));

            if (!File.Exists(pdfFilePath))
                throw new InvalidOperationException("Brak pliku PDF");

            _parsedPdf = new Dictionary<int, SlideData>();
            _songEdges = new Dictionary<int, int>();

            ReadTextFromPdfToMemory(pdfFilePath);
            BuildSongsList(pdfFilePath);
        }

        public void WriteParsedTextToDatabase(string databaseFilePath)
        {
            using (var db = new LiteDatabase(databaseFilePath + ".db"))
            {
                var songsCollection = db.GetCollection<SongData>("songs");
                var songPagesCollection = db.GetCollection<SlideData>("songPages");
                
                // Slajd Zaciemnienie
                var zaciemnienie = new SongData
                {
                    Indexable = false,
                    Name = "Zaciemnienie",
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    Protected = true
                };

                songsCollection.Insert(zaciemnienie);
                songPagesCollection.Insert(new SlideData
                {
                    SongId = zaciemnienie.Id,
                    Order = 1,
                    PageText = File.ReadAllText(@"D:\Efraim\SlajdyStartowe\Zaciemnienie.html"),
                    CustomHtml = true,
                    Created = DateTime.Now,
                    Modified = DateTime.Now
                });

                // Slajd Logo Efraim
                var logoEfraim = new SongData
                {
                    Indexable = false,
                    Name = "Logo Efraim",
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    Protected = true
                };

                songsCollection.Insert(logoEfraim);
                songPagesCollection.Insert(new SlideData
                {
                    SongId = logoEfraim.Id,
                    Order = 1,
                    PageText = File.ReadAllText(@"D:\Efraim\SlajdyStartowe\Logo.html"),
                    CustomHtml = true,
                    Created = DateTime.Now,
                    Modified = DateTime.Now
                });

                // Slajd Słowo Efraim
                var slowoEfraim = new SongData
                {
                    Indexable = true,
                    Name = "Słowo Efraim",
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    Protected = true
                };

                songsCollection.Insert(slowoEfraim);
                songPagesCollection.Insert(new SlideData
                {
                    SongId = slowoEfraim.Id,
                    Order = 1,
                    PageText = SearchHelper.PrepareSongTextToDatabase(_parsedPdf.First(x => x.Key == 2).Value.PageText),
                    SearchPageText = $"{slowoEfraim.Name} {_parsedPdf.First(x => x.Key == 2).Value.SearchPageText}".Trim(),
                    Created = DateTime.Now,
                    Modified = DateTime.Now
                });

                // Slajd Testowy
                var slajdTestowy = new SongData
                {
                    Indexable = true,
                    Name = "Slajd testowy",
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    Protected = true
                };

                songsCollection.Insert(slajdTestowy);
                songPagesCollection.Insert(new SlideData
                {
                    SongId = slajdTestowy.Id,
                    Order = 1,
                    PageText = SearchHelper.PrepareSongTextToDatabase(_parsedPdf.First(x => x.Key == 3).Value.PageText),
                    SearchPageText =  _parsedPdf.First(x => x.Key == 3).Value.SearchPageText,
                    Created = DateTime.Now,
                    Modified = DateTime.Now
                });

                // Slajd Maryja
                var slajdMaryja = new SongData
                {
                    Indexable = false,
                    Name = "Maryja",
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    Protected = true
                };

                songsCollection.Insert(slajdMaryja);
                songPagesCollection.Insert(new SlideData
                {
                    SongId = slajdMaryja.Id,
                    Order = 1,
                    PageText = File.ReadAllText(@"D:\Efraim\SlajdyStartowe\Maryja.html"),
                    CustomHtml = true,
                    Created = DateTime.Now,
                    Modified = DateTime.Now
                });

                var songCounter = 0;
                var songNames = File.ReadAllLines(@"D:\Piesni_Import.txt");

                foreach (var songEdge in _songEdges)
                {
                    var songName = "Pieśń " + (songCounter + 1);
                    var paperSongBookId = string.Empty;

                    if (songCounter < songNames.Length)
                    {
                        var splittedSlide = songNames[songCounter].Split(new[] {'.'}, StringSplitOptions.RemoveEmptyEntries);

                        if (splittedSlide.Length < 2)
                            throw new Exception("Błąd importu");

                        songName = string.Join("", splittedSlide.Skip(1));
                        paperSongBookId = splittedSlide[0];
                    }

                    else
                    {
                        var songSlides = _parsedPdf.Where(x => x.Key >= songEdge.Key && x.Key <= songEdge.Value).ToArray();

                        if (songSlides.Any())
                        {
                            var splittedSlide = songSlides.ElementAt(0).Value.PageText.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                            songName = splittedSlide.Length > 1
                                ? splittedSlide[0].Trim() + " " + splittedSlide[1].Trim()
                                : splittedSlide[0].Trim();
                        }
                    }

                    var songId = songsCollection.Insert(new SongData
                    {
                        Indexable = true,
                        Name = songName,
                        NameNormalized = SearchHelper.NormalizeText(songName),
                        PaperSongBookId = paperSongBookId,
                        Created = DateTime.Now,
                        Modified = DateTime.Now
                    });

                    var slideCounter = 0;

                    songPagesCollection.Insert(_parsedPdf.Where(x => x.Key >= songEdge.Key && x.Key <= songEdge.Value).Select(y => new SlideData
                    {
                        SearchPageText = slideCounter == 0 ?
                            $"{SearchHelper.NormalizeText(paperSongBookId)} {SearchHelper.NormalizeText(songName)} {SearchHelper.NormalizeText(y.Value.SearchPageText ?? string.Empty)}".Trim() :
                            y.Value.SearchPageText,
                        PageText = SearchHelper.PrepareSongTextToDatabase(y.Value.PageText),
                        SongId = songId.AsInt32,
                        Order = ++slideCounter,
                        Created = DateTime.Now,
                        Modified = DateTime.Now
                    }));

                    songCounter++;
                }

                songsCollection.EnsureIndex(x => x.Id, true);
                songsCollection.EnsureIndex(x => x.Name);
                songPagesCollection.EnsureIndex(x => x.Id, true);
                songPagesCollection.EnsureIndex(x => x.SongId);
            }
        }

        private void ReadTextFromPdfToMemory(string pdfFilePath)
        {
            var pdfReader = new PdfReader(pdfFilePath);
            
            for (var page = 1; page <= pdfReader.NumberOfPages; page++)
            {
                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();

                string currentPageText = PdfTextExtractor.GetTextFromPage(pdfReader, page, strategy);
                _parsedPdf.Add(page, new SlideData
                {
                    //PageNumber = page,
                    SearchPageText = SearchHelper.NormalizeText(currentPageText),
                    PageText = currentPageText,
                   // CurrentSongEndPage = 0
                });
            }

            pdfReader.Close();
        }

        private void BuildSongsList(string pdfFilePath)
        {
            var pdfReader = new PdfReader(pdfFilePath);
            var firstPage = 0;

            for (var page = 1; page <= pdfReader.NumberOfPages; page++)
            {
                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();

                string currentPageText = PdfTextExtractor.GetTextFromPage(pdfReader, page, strategy).ToLower();

                var integerValue = currentPageText
                    .Split(new[] { ' ', '\n' }, StringSplitOptions.RemoveEmptyEntries)
                    .Where(x => x.Length == 3 && int.TryParse(x, out _));

                if (integerValue.Count() == 1)
                {
                    if (firstPage == 0)
                        firstPage = page;
                    else
                    {
                        _songEdges.Add(firstPage, page - 1);
                        firstPage = page;
                    }
                }
            }

            pdfReader.Close();
        }
    }
}
