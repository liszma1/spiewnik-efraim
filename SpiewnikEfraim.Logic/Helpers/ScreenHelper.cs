﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;

namespace SpiewnikEfraim.Logic.Helpers
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct Rect
    {
        public int left;
        public int top;
        public int right;
        public int bottom;
    }

    public class MonitorInfo
    {
        public bool IsPrimary;
        public string DeviceName;
        public Rectangle Bounds;
    }

    public class ScreenHelper
    {
        [DllImport("user32")]
        private static extern bool EnumDisplayMonitors(IntPtr hdc, IntPtr lpRect, MonitorEnumProc callback, int dwData);

        private delegate bool MonitorEnumProc(IntPtr hDesktop, IntPtr hdc, ref Rect pRect, int dwData);

        public MonitorInfo[] GetAllScreens()
        {
            var currentScreens = new List<MonitorInfo>();

            bool Callback(IntPtr hDesktop, IntPtr hdc, ref Rect prect, int d)
            {
                currentScreens.Add(new MonitorInfo
                {
                    Bounds = new Rectangle
                    {
                        X = prect.left,
                        Y = prect.top,
                        Width = prect.right - prect.left,
                        Height = prect.bottom - prect.top,
                    },
                    IsPrimary = prect.left == 0 && prect.top == 0,
                    DeviceName = "Monitor " + (currentScreens.Count + 1)
                });

                return true;
            }

            EnumDisplayMonitors(IntPtr.Zero, IntPtr.Zero, Callback, 0);
            return currentScreens.ToArray();
        }


        public int GetMonitorsCount()
        {
            return GetAllScreens().Length;
        }

        public MonitorInfo GetMonitorProperties(int screenNumber)
        {
            var allScreens = GetAllScreens();

            if (allScreens.Length < screenNumber)
                return null;

            return allScreens[screenNumber - 1];
        }

        public MonitorInfo GetNonPrimaryMonitorProperties()
        {
            return GetAllScreens().FirstOrDefault(x => !x.IsPrimary);
        }

        public MonitorInfo GetPrimaryMonitorProperties()
        {
            return GetAllScreens().FirstOrDefault(x => x.IsPrimary);
        }
    }
}
