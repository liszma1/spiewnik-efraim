﻿using System.Runtime.InteropServices;

namespace SpiewnikEfraim.Logic.Helpers
{
    public class TaskbarHelper
    {
        [DllImport("user32.dll")]
        private static extern int FindWindow(string className, string windowText);

        [DllImport("user32.dll")]
        private static extern int ShowWindow(int hwnd, int command);

        [DllImport("user32.dll")]
        public static extern int FindWindowEx(int parentHandle, int childAfter, string className, int windowTitle);

        [DllImport("user32.dll")]
        private static extern int GetDesktopWindow();

        private const int SwHide = 0;
        private const int SwShow = 1;

        protected static int Handle => FindWindow("Shell_TrayWnd", "");
        protected static int HandleOfStartButton
        {
            get
            {
                var handleOfDesktop = GetDesktopWindow();
                var handleOfStartButton = FindWindowEx(handleOfDesktop, 0, "button", 0);
                return handleOfStartButton;
            }
        }

        private TaskbarHelper()
        {
            // hide ctor
        }

        public static void Show()
        {
            ShowWindow(Handle, SwShow);
            ShowWindow(HandleOfStartButton, SwShow);
        }

        public static void Hide()
        {
            ShowWindow(Handle, SwHide);
            ShowWindow(HandleOfStartButton, SwHide);
        }
    }
}
