﻿using System.Net;

namespace SpiewnikEfraim.Logic.Services
{
    public interface IConfigProvider
    {
        string DatabasePath { get; }
        string SlideTemplate { get; }
        IPAddress TvAddress { get; }
        int TvPort { get; }
        bool IntegrationDisabled { get; }
    }
}
