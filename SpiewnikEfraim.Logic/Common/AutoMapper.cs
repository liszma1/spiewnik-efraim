﻿using AutoMapper;
using SpiewnikEfraim.Logic.Dto;
using SpiewnikEfraim.Logic.ViewModels;

namespace SpiewnikEfraim.Logic.Common
{
    public static class AutoMapper
    {
        public static void Init()
        {            
            Mapper.Initialize(cfg =>
            {
                cfg.DisableConstructorMapping();
                cfg.CreateMap<SlideData, SlideDataVm>()
                    .ForMember(x => x.SongName, opt => opt.Ignore())
                    .ForMember(x => x.AdHoc, opt => opt.Ignore());
            });
            CheckConfiguration();
        }

        public static void CheckConfiguration()
        {
            Mapper.AssertConfigurationIsValid();
        }
    }
}
