﻿namespace SpiewnikEfraim.Logic.Dto
{
    public enum ErrorStatus
    {
        Ok = 0,
        RegistryKeyNotFound = 1,
        RegistryKeySaveFailed = 2,
        UndefinedError = 3
    }
}
