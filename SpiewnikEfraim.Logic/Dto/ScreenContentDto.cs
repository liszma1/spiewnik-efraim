﻿namespace SpiewnikEfraim.Logic.Dto
{
    public class ScreenContentDto
    {
        public int? SongId { get; set; }
        public int? SlideId { get; set; }
        public bool AdHoc { get; set; }
        public string SlideContent { get; set; }
    }
}