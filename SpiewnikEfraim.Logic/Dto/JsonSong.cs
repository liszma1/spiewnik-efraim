﻿using System;

namespace SpiewnikEfraim.Logic.Dto
{
    public class JsonSong
    {
        public string PaperSongBookId { get; set; }
        public string Name { get; set; }
        public bool Indexable { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public JsonSlide[] Slides { get; set; }
    }
}
