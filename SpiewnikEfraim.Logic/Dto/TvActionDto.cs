﻿namespace SpiewnikEfraim.Logic.Dto
{
    public enum TvActionDto
    {
        TurnOff = 0,
        TurnOn = 1,
        SwitchToHdmi1 = 2,
        SwitchToHdmi2 = 3,
        SwitchToHdmi3 = 4
    }
}
