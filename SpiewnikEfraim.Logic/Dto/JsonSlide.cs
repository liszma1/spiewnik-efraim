﻿using System;

namespace SpiewnikEfraim.Logic.Dto
{
    public class JsonSlide
    {
        public int Order { get; set; }
        public string PageText { get; set; }
        public bool CustomHtml { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
