﻿using System.Runtime.Serialization;

namespace SpiewnikEfraim.Logic.Dto
{
    [DataContract]
    public class RectangleData
    {
        [DataMember]
        public int Right { get; set; }

        [DataMember]
        public int Top { get; set; }

        [DataMember]
        public int Left { get; set; }

        [DataMember]
        public int Height { get; set; }

        [DataMember]
        public int Width { get; set; }

        [DataMember]
        public int Y { get; set; }

        [DataMember]
        public int X { get; set; }

        [DataMember]
        public int Bottom { get; set; }
    }
}
