﻿using System;

namespace SpiewnikEfraim.Logic.Dto
{
    public class SongData
    {
        public int Id { get; set; }
        public string PaperSongBookId { get; set; }
        public string Name { get; set; }
        public string NameNormalized { get; set; }
        public bool Indexable { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool Protected { get; set; }
    }
}
