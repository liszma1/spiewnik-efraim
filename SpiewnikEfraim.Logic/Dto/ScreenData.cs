﻿using System.Runtime.Serialization;

namespace SpiewnikEfraim.Logic.Dto
{
    [DataContract]
    public class ScreenData
    {
        [DataMember]
        public bool Primary { get; set; }

        [DataMember]
        public string DeviceName { get; set; }

        [DataMember]
        public RectangleData Bounds { get; set; }

        [DataMember]
        public int BitsPerPixel { get; set; }
    }
}
