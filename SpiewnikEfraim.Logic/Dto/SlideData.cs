﻿using System;

namespace SpiewnikEfraim.Logic.Dto
{
    public class SlideData
    {
        public int Id { get; set; }
        public int SongId { get; set; }
        public int Order { get; set; }
        public string PageText { get; set; }
        public string SearchPageText { get; set; }
        public bool CustomHtml { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
