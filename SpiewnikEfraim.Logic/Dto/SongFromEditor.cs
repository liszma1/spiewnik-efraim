﻿namespace SpiewnikEfraim.Logic.Dto
{
    public class SongFromEditor
    {
        public SongData SongData { get; set; }
        public SlideData[] SlidesData { get; set; }
    }
}
