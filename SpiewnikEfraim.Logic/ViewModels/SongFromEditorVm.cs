﻿namespace SpiewnikEfraim.Logic.ViewModels
{
    public class SongFromEditorVm
    {
        public SongDataVm SongData { get; set; }
        public SlideDataVm[] SlidesData { get; set; }
    }
}
