﻿using System;

namespace SpiewnikEfraim.Logic.ViewModels
{
    public class SongDataVm
    {
        public int Id { get; set; }
        public string PaperSongBookId { get; set; }
        public string Name { get; set; }
        public string NameNormalized { get; set; }
        public bool Indexable { get; set; }
        public int SlidesCount { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public bool Protected { get; set; }
    }
}