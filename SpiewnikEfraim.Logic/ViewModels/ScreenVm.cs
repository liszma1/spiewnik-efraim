﻿namespace SpiewnikEfraim.Logic.ViewModels
{
    public class ScreenVm
    {
        public int SlideId { get; set; }
        public string SongName { get; set; }
        public bool RefreshDisplayDriver { get; set; }
    }
}