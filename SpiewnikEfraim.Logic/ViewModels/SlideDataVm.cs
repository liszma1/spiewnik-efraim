﻿using System;

namespace SpiewnikEfraim.Logic.ViewModels
{
    public class SlideDataVm
    {
        public int Id { get; set; }
        public int SongId { get; set; }
        public string SongName { get; set; }
        public string PageText { get; set; }
        public int Order { get; set; }
        public string SearchPageText { get; set; }
        public bool CustomHtml { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public bool? AdHoc { get; set; }
    }
}