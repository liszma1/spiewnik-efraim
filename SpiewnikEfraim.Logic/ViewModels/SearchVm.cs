﻿namespace SpiewnikEfraim.Logic.ViewModels
{
    public class SearchVm
    {
        public string TextToFind { get; set; }
        public int SlideId { get; set; }
        public int SongId { get; set; }
        public bool SkipResultsFromSameSong { get; set; }
        public bool SkipHtmlIfNotCustom { get; set; }
    }
}