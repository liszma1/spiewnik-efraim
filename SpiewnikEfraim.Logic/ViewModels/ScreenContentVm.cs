﻿namespace SpiewnikEfraim.Logic.ViewModels
{
    public class ScreenContentVm
    {
        public int? SongId { get; set; }
        public int? SlideId { get; set; }
        public bool AdHoc { get; set; }
        public string SlideContent { get; set; }

        public ScreenContentVm()
        {
            SongId = 0;
            SlideContent = string.Empty;
        }
    }
}