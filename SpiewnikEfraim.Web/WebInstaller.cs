﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using SpiewnikEfraim.Logic.Services;
using SpiewnikEfraim.Web.Services;
using SpiewnikEfraim.Logic.Helpers;

namespace SpiewnikEfraim.Web
{
    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                .Pick().If(t => t.Name.EndsWith("Controller"))
                .Configure(configurer => configurer.Named(configurer.Implementation.Name))
                .LifestylePerWebRequest());

            container.Register(Component
                .For<IConfigProvider>()
                .ImplementedBy<ConfigProvider>().LifestyleSingleton());

            container.Register(Component
                .For<IDbHelper>()
                .ImplementedBy<DbHelper>().LifestyleSingleton());

            container.Register(Component
                .For<ITvHelper>()
                .ImplementedBy<TvHelper>().LifestyleSingleton());
        }
    }
}