﻿using System;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using Castle.Windsor;

namespace SpiewnikEfraim.Web.IoC
{
    public class WebApiFactory : IHttpControllerActivator
    {
        private readonly IWindsorContainer _container;

        public WebApiFactory(IWindsorContainer container)
        {
            if (container == null)
                throw new ArgumentNullException(nameof(container));

            _container = container;
        }

        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            if (controllerType == null)
                throw new HttpException(404, "Ścieżka do kontrolera WebApi nie została odnaleziona.");

            var webApiController = (IHttpController)_container.Resolve(controllerType);
            request.RegisterForDispose(new WebApiControllerReleaser(() => _container.Release(webApiController)));

            return webApiController;
        }
    }

    internal class WebApiControllerReleaser : IDisposable
    {
        private readonly Action _releaseAction;

        public WebApiControllerReleaser(Action release)
        {
            _releaseAction = release;
        }

        public void Dispose()
        {
            _releaseAction();
        }
    }
}