﻿using System;
using System.Reflection;
using Castle.Core.Internal;
using Castle.MicroKernel.Registration;
using Castle.Windsor.Installer;

namespace SpiewnikEfraim.Web.IoC
{
    internal class EfraimInstallerFactory : InstallerFactory
    {
        private readonly IInstallerSettings _installerSettings;

        public EfraimInstallerFactory(IInstallerSettings installerSettings)
        {
            _installerSettings = installerSettings;
        }

        public override IWindsorInstaller CreateInstance(Type installerType)
        {
            return InstallerOczekujeUstawien(installerType)
                ? UtworzInstallerZUstawieniami(installerType)
                : UtworzInstallerBezUstawien(installerType);
        }

        private bool InstallerOczekujeUstawien(Type installerType)
        {
            var types = new[] { typeof(IInstallerSettings) };
            var constructor = installerType.GetConstructor(BindingFlags.Instance | BindingFlags.Public, null, types, null);

            return constructor != null;
        }

        private IWindsorInstaller UtworzInstallerZUstawieniami(Type installerType)
        {
            if (_installerSettings == null)
                throw new InvalidOperationException($"Nie podano ustawień, a Installer {installerType.FullName} ich oczekuje");

            return installerType.CreateInstance<IWindsorInstaller>(new object[] { _installerSettings });
        }

        private IWindsorInstaller UtworzInstallerBezUstawien(Type installerType)
        {
            return installerType.CreateInstance<IWindsorInstaller>(new object[0]);
        }
    }
}