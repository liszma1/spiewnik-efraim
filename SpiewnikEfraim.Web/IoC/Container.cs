﻿using System;
using Castle.Windsor;

namespace SpiewnikEfraim.Web.IoC
{
    public static class Container
    {
        private static readonly object LockObject = new object();
        public static WindsorContainer WindsorContainer { get; private set; }

        private static readonly IInstallerSettings Settings = new InstallerSettingsFromWebConfig();

        public static void Init()
        {
            SetupIfNeeded();
        }

        public static void Dispose()
        {
            lock (LockObject)
            {
                if (!IsSetup())
                    return;

                WindsorContainer.Dispose();
                WindsorContainer = null;
            }
        }

        private static bool IsSetup()
        {
            return WindsorContainer != null;
        }
        private static void SetupIfNeeded()
        {
            try
            {
                lock (LockObject)
                {
                    if (!IsSetup())
                    {
                        WindsorContainer = new WindsorContainer();
                        WindsorContainer.Install(EfraimAssemblies.InThisApplication(new EfraimInstallerFactory(Settings)));
                    }
                }
            }
            catch (Exception)
            {
                WindsorContainer = null;
                throw;
            }
        }
        
    }
}
