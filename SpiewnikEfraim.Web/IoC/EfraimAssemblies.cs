﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Castle.MicroKernel.Registration;
using Castle.Windsor.Installer;

namespace SpiewnikEfraim.Web.IoC
{
    internal static class EfraimAssemblies
    {
        public static IWindsorInstaller InThisApplication(InstallerFactory installerFactory)
        {
            var compositeInstaller = new CompositeInstaller();

            foreach (var assembly in GetEfraimAssemblies())
                compositeInstaller.Add(FromAssembly.Instance(assembly, installerFactory));

            return compositeInstaller;
        }

        private static IEnumerable<Assembly> GetEfraimAssemblies()
        {
            var assemblies = new HashSet<Assembly>();

            foreach (var efraimAssembly in AppDomain.CurrentDomain.GetAssemblies().Where(a => IsEfraimAssembly(a.FullName)))
                AddEfraimAssemblies(efraimAssembly, assemblies);

            return assemblies;
        }

        private static void AddEfraimAssemblies(Assembly assembly, HashSet<Assembly> assemblies)
        {
            var referencedEfraimAssemblies = assembly.GetReferencedAssemblies()
                .Where(assemblyName => IsEfraimAssembly(assemblyName.FullName));

            foreach (var assemblyName in referencedEfraimAssemblies)
                AddEfraimAssemblies(Assembly.Load(assemblyName), assemblies);

            assemblies.Add(assembly);
        }

        private static bool IsEfraimAssembly(string assemblyName)
        {
            return assemblyName.ToLower().StartsWith("spiewnikefraim");
        }
    }
}