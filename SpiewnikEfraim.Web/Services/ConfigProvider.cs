﻿using System;
using System.IO;
using System.Configuration;
using System.Net;
using System.Web;
using SpiewnikEfraim.Logic.Services;

namespace SpiewnikEfraim.Web.Services
{
    public class ConfigProvider : IConfigProvider
    {
        private string _databasePath;
        public string DatabasePath
        {
            get
            {
                if (!string.IsNullOrEmpty(_databasePath))
                    return _databasePath;

                _databasePath = HttpContext.Current.Server.MapPath("~/App_Data/data.db");

                if (string.IsNullOrWhiteSpace(_databasePath))
                    throw new ArgumentNullException(nameof(_databasePath));

                return _databasePath;
            }
        }

        private bool? _disableIntegration;
        public bool IntegrationDisabled
        {
            get
            {
                if (_disableIntegration != null)
                    return _disableIntegration.Value;

                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["DisableCommunicationWithScreenDriver"]))
                    throw new ArgumentNullException(nameof(_disableIntegration));

                if (!bool.TryParse(ConfigurationManager.AppSettings["DisableCommunicationWithScreenDriver"], out var result))
                    throw new InvalidOperationException();

                _disableIntegration = result;
                return result;
            }
        }

        private string _slideTemplate;
        public string SlideTemplate
        {
            get
            {
                if (!string.IsNullOrEmpty(_slideTemplate))
                    return _slideTemplate;

                var slideTemplatePath = HttpContext.Current.Server.MapPath("~/App_Data/Template.html");

                if (string.IsNullOrWhiteSpace(slideTemplatePath))
                    throw new ArgumentNullException(nameof(slideTemplatePath));

                if (!File.Exists(slideTemplatePath))
                    throw new InvalidOperationException(slideTemplatePath + " nie istnieje");

                _slideTemplate = File.ReadAllText(slideTemplatePath);
                return _slideTemplate;
            }
        }

        private IPAddress _tvAddress = null;
        public IPAddress TvAddress
        {
            get
            {
                if (_tvAddress != null)
                    return _tvAddress;

                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["TvAddress"]))
                    throw new ArgumentNullException(nameof(_tvAddress));

                if (!IPAddress.TryParse(ConfigurationManager.AppSettings["TvAddress"], out var result))
                    throw new InvalidOperationException();

                _tvAddress = result;
                return _tvAddress;
            }
        }

        private int _tvPort;
        public int TvPort
        {
            get
            {
                if (_tvPort != 0)
                    return _tvPort;

                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["TvPort"]))
                    throw new ArgumentNullException(nameof(_tvPort));

                if (!int.TryParse(ConfigurationManager.AppSettings["TvPort"], out var result))
                    throw new InvalidOperationException();

                _tvPort = result;
                return _tvPort;
            }
        }
    }
}
