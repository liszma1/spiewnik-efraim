﻿using Owin;
using Microsoft.Owin;

[assembly: OwinStartup(typeof(SpiewnikEfraim.Web.RefreshLogic.RefreshHost))]

namespace SpiewnikEfraim.Web.RefreshLogic
{
    public class RefreshHost
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}