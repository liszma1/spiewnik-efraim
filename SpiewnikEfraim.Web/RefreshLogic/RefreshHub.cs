﻿using Microsoft.AspNet.SignalR;

namespace SpiewnikEfraim.Web.RefreshLogic
{
    public class RefreshHub : Hub
    {
        public void RefreshSlideNumber(int pageNumber)
        {
            Clients.All.refreshSlideNumber(pageNumber);
        }
    }
}