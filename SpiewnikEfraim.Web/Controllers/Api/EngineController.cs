﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using AutoMapper;
using System.Web.Http;
using Newtonsoft.Json;
using SpiewnikEfraim.Logic.Dto;
using SpiewnikEfraim.Logic.Helpers;
using SpiewnikEfraim.Logic.Services;
using SpiewnikEfraim.Logic.ViewModels;

namespace SpiewnikEfraim.Web.Controllers.Api
{
    [RoutePrefix("api")]
    public class EngineController : ApiController
    {
        private readonly IDbHelper _dbHelper;
        private readonly IConfigProvider _configurationProvider;

        public EngineController(IDbHelper dbHelper, IConfigProvider configurationProvider)
        {
            _dbHelper = dbHelper ?? throw new ArgumentNullException(nameof(dbHelper));
            _configurationProvider = configurationProvider ?? throw new ArgumentNullException(nameof(configurationProvider));
        }

        [HttpPost]
        [Route("deleteSongFromDatabase")]
        public bool DeleteSongFromDatabase([FromBody] SearchVm searchVm)
        {
            if (searchVm == null)
                throw new ArgumentNullException(nameof(searchVm));

            return _dbHelper.RemoveSong(searchVm.SongId);
        }

        [HttpPost]
        [Route("saveSongToDatabase")]
        public int SaveSongToDatabase([FromBody] SongFromEditorVm songFromEditor)
        {
            if (songFromEditor == null)
                throw new ArgumentNullException(nameof(songFromEditor));

            return _dbHelper.SaveSong(Mapper.Map<SongFromEditorVm, SongFromEditor>(songFromEditor));
        }

        [HttpPost]
        [Route("rebuildDatabase")]
        public bool RebuildDatabase()
        {
            return _dbHelper.RebuildDatabase();
        }

        [HttpGet]
        [Route("getSlideTemplate")]
        public string GetSlideTemplate()
        {
            return _configurationProvider.SlideTemplate.Replace("{{", "{").Replace("}}", "}");
        }

        [HttpGet]
        [Route("exportSongAsHtml")]
        public HttpResponseMessage ExportSongAsHtml(int songId)
        {
            var pushStreamContent = new PushStreamContent(async (stream, content, context) =>
            {
                using (var zippedFiles = _dbHelper.ExportSongAsZippedHtml(songId))
                {
                    if (zippedFiles == null)
                        return;

                    await zippedFiles.CopyToAsync(stream);
                    stream.Close();
                }
            }, "application/zip");

            return new HttpResponseMessage(HttpStatusCode.OK) { Content = pushStreamContent };
        }

        [HttpGet]
        [Route("exportSongAsJson")]
        public HttpResponseMessage ExportSongAsJson(int songId)
        {
            var result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StringContent(JsonConvert.SerializeObject(new[] { _dbHelper.ExportSongAsJson(songId) }));
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return result;
        }

        [HttpGet]
        [Route("exportSongsAsHtml")]
        public HttpResponseMessage ExportSongsAsHtml()
        {
            var pushStreamContent = new PushStreamContent(async (stream, content, context) =>
            {
                using (var zippedFiles = _dbHelper.ExportSongsAsZippedHtml())
                {
                    await zippedFiles.CopyToAsync(stream);
                    stream.Close();
                }
            }, "application/zip");

            return new HttpResponseMessage(HttpStatusCode.OK) { Content = pushStreamContent };
        }

        [HttpGet]
        [Route("exportSongsAsJson")]
        public HttpResponseMessage ExportSongsAsJson()
        {
            var result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StringContent(JsonConvert.SerializeObject(_dbHelper.ExportSongsAsJson()));
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return result;
        }

        [HttpPost]
        [Route("importSongsFromJson")]
        public string[] ImportSongsFromJson([FromUri] bool forceSave)
        {
            var httpRequest = HttpContext.Current.Request;

            if (httpRequest.Files.Count <= 0)
                return null;

            var result = new List<string>();

            for(var i = 0; i < httpRequest.Files.Count; i++)
            {
                using (var binaryReader = new BinaryReader(httpRequest.Files[i].InputStream))
                {
                    result.AddRange(_dbHelper.ImportSongFromJson(System.Text.Encoding.UTF8.GetString(binaryReader.ReadBytes(httpRequest.Files[i].ContentLength)), forceSave));
                }
            }

            return result.ToArray();
        }
    }
}