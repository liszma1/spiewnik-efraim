﻿using System;
using System.Web.Http;
using AutoMapper;
using SpiewnikEfraim.Logic.Dto;
using SpiewnikEfraim.Logic.Helpers;
using SpiewnikEfraim.Web.ScreenService;
using SpiewnikEfraim.Logic.Services;
using SpiewnikEfraim.Logic.ViewModels;

namespace SpiewnikEfraim.Web.Controllers.Api
{
    [RoutePrefix("api")]
    public class ScreenController : ApiController
    {
        private readonly ScreenServiceClient _screenService;
        private readonly IDbHelper _dbHelper;
        private readonly IConfigProvider _configProvider;
        private readonly ITvHelper _tvHelper;

        public ScreenController(IDbHelper dbHelper, IConfigProvider configProvider, ITvHelper tvHelper)
        {
            _configProvider = configProvider ?? throw new ArgumentNullException(nameof(configProvider));
            _tvHelper = tvHelper ?? throw new ArgumentNullException(nameof(tvHelper));
            _dbHelper = dbHelper ?? throw new ArgumentNullException(nameof(dbHelper));
            _screenService = new ScreenServiceClient();
        }

        private bool CheckIsServiceAvailable()
        {
            try
            {
                if (!_screenService.ServiceAvailable())
                    return false;
            }
            catch
            {
                return false;
            }

            return true;
        }

        [HttpPost]
        [Route("getSlideContentFromExistingScreenProcess")]
        public SlideDataVm GetSlideContentFromExistingScreenProcess()
        {
            if (_configProvider.IntegrationDisabled)
                return new SlideDataVm {SongId = -1};

            if (!CheckIsServiceAvailable())
                return null;

            var currentContent = _screenService.GetCurrentSlideContent();

            if (currentContent == null)
                return null;

            return Mapper.Map<SlideData, SlideDataVm>(currentContent);
        }

        [HttpPost]
        [Route("setPrimaryMonitor")]
        public bool SetPrimaryMonitor()
        {
            if (_configProvider.IntegrationDisabled)
                return true;

            if (!CheckIsServiceAvailable())
                return false;

            return _screenService.SetScreenPrimaryMonitorFirst();
        }

        [HttpPost]
        [Route("setSecondaryMonitor")]
        public bool SetSecondaryMonitor()
        {
            if (_configProvider.IntegrationDisabled)
                return true;

            if (!CheckIsServiceAvailable())
                return false;

            return _screenService.SetScreenNonPrimaryMonitorFirst();
        }

        [HttpPost]
        [Route("startScreenProcess")]
        public StartScreenProcessVm StartScreenProcess([FromBody] ScreenVm screenVm)
        {
            if (_configProvider.IntegrationDisabled)
                return StartScreenProcessVm.Ok;

            if (!CheckIsServiceAvailable())
                return StartScreenProcessVm.ScreenServiceNotAvailable;

            if (_screenService.GetMonitorsCount() == 0)
                return StartScreenProcessVm.NoDisplayAvailable;
                                    
            if (screenVm != null && screenVm.RefreshDisplayDriver)
            {
                if (!_screenService.SetScreenPrimaryMonitorFirst())
                    return StartScreenProcessVm.ResizeScreenError;
            }

            return StartScreenProcessVm.Ok;
        }
        
        [HttpPost]
        [Route("setSpecifiedContentOnScreen")]
        public bool SetSpecifiedContentOnScreen([FromBody] SlideDataVm slideDataVm)
        {
            if (_configProvider.IntegrationDisabled)
                return true;

            if (!CheckIsServiceAvailable())
                return false;

            var slideData = slideDataVm.AdHoc ?? false ? 
                Mapper.Map<SlideDataVm, SlideData>(slideDataVm) : 
                _dbHelper.GetSlideData(slideDataVm.Id);

            if (slideData == null)
                return false;

            slideData.PageText = slideData.CustomHtml
                    ? slideData.PageText
                    : string.Format(_configProvider.SlideTemplate, slideData.PageText);

            _screenService.SetScreenContent(slideData);

            return true;
        }

        [HttpPost]
        [Route("setSpecifiedContentOnScreenBySongName")]
        public int SetSpecifiedContentOnScreenBySongName([FromBody] ScreenVm screenVm)
        {
            if (_configProvider.IntegrationDisabled)
                return 0;

            if (!CheckIsServiceAvailable())
                return -1;

            var slideData = _dbHelper.GetSlideDataBySongName(screenVm.SongName);

            if (slideData == null)
                return -1;

            _screenService.SetScreenContent(slideData);

            return slideData.Id;
        }

        [HttpPost]
        [Route("getMonitorsInfo")]
        public ScreenData[] GetMonitorsInfo()
        {
            if (_configProvider.IntegrationDisabled)
                return new ScreenData[0];

            if (!CheckIsServiceAvailable())
                return null;

            return _screenService.GetMonitorsProperties();
        }

        [HttpPost]
        [Route("shutdownServer")]
        public void ShutdownServer()
        {
            if (_configProvider.IntegrationDisabled)
                return;

            _screenService.ShutdownServer();
        }

        [HttpPost]
        [Route("restartServer")]
        public void RestartServer()
        {
            if (_configProvider.IntegrationDisabled)
                return;

            _screenService.RestartServer();
        }

        [HttpPost]
        [Route("executeActionOnTv")]
        public bool ExecuteActionOnTv([FromUri]TvActionDto actionToExecute)
        {
            return _tvHelper.ExecuteAction(actionToExecute);
        }
    }
}
