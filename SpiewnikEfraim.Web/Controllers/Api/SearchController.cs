﻿using System;
using System.Linq;
using AutoMapper;
using System.Web.Http;
using SpiewnikEfraim.Logic.Helpers;
using SpiewnikEfraim.Logic.Dto;
using SpiewnikEfraim.Logic.Services;
using SpiewnikEfraim.Logic.ViewModels;

namespace SpiewnikEfraim.Web.Controllers.Api
{
    [RoutePrefix("api")]
    public class SearchController : ApiController
    {
        private readonly IDbHelper _dbHelper;
        private readonly IConfigProvider _configurationProvider;

        public SearchController(IDbHelper dbHelper, IConfigProvider configurationProvider)
        {
            _dbHelper = dbHelper ?? throw new ArgumentNullException(nameof(dbHelper));
            _configurationProvider = configurationProvider ?? throw new ArgumentNullException(nameof(configurationProvider));
        }

        [HttpPost]
        [Route("searchForSlidesAndGetContent")]
        public SlideDataVm[] SearchForSlidesAndGetContent([FromBody] SearchVm searchVm)
        {
            if (searchVm == null || string.IsNullOrWhiteSpace(searchVm.TextToFind))
                return new SlideDataVm[0];

            var resultObject = Mapper.Map<SlideData[], SlideDataVm[]>(
                _dbHelper.SearchInSlides(SearchHelper.NormalizeText(searchVm.TextToFind), searchVm.SkipResultsFromSameSong));

            Array.ForEach(resultObject, x =>
            {
                x.SongName = _dbHelper.GetSongDataById(x.SongId)?.Name;
                x.PageText = x.CustomHtml ? x.PageText : string.Format(_configurationProvider.SlideTemplate, x.PageText);
            }); // TODO: Move to AutoMapper resolver
            return resultObject;
        }

        [HttpPost]
        [Route("getSlideData")]
        public SlideDataVm GetSlideData([FromBody] SearchVm searchVm)
        {
            var resultObject = Mapper.Map<SlideData, SlideDataVm>(_dbHelper.GetSlideData(searchVm.SlideId));
            resultObject.PageText = resultObject.CustomHtml ? resultObject.PageText : string.Format(_configurationProvider.SlideTemplate, resultObject.PageText); // TODO!!!
            return resultObject;
        }

        [HttpPost]
        [Route("getSongDataBySongId")]
        public SlideDataVm[] GetSongDataBySongId([FromBody] SearchVm searchVm)
        {
            var resultObject = Mapper.Map<SlideData[], SlideDataVm[]>(
                _dbHelper.GetSongSlidesById(searchVm.SongId));
            
            Array.ForEach(resultObject, x => x.PageText = x.CustomHtml ? x.PageText : searchVm.SkipHtmlIfNotCustom ? x.PageText : string.Format(_configurationProvider.SlideTemplate, x.PageText)); // TODO!!!
            return resultObject;
        }

        [HttpGet]
        [Route("getSpecialSongs")]
        public SlideDataVm[] GetSpecialSongs()
        {
            return _dbHelper.GetSpecialSongs().Select(x =>
            {
                var result = _dbHelper.GetSlideDataBySongName(x.Name);

                if (result != null)
                    return new SlideDataVm
                    {
                        Created = result.Created,
                        Id = result.Id,
                        PageText = result.CustomHtml ? result.PageText : string.Format(_configurationProvider.SlideTemplate, result.PageText),
                        Order = result.Order,
                        Modified = result.Modified,
                        SearchPageText = result.SearchPageText,
                        SongId = result.SongId,
                        CustomHtml = result.CustomHtml,
                        SongName = x.Name
                    };

                return null;
            }).ToArray();
        }

        [HttpGet]
        [Route("getAllSongs")]
        public SongDataVm[] GetAllSongs()
        {
            var songs = _dbHelper.GetAllSongs();
            var songSlides = _dbHelper.GetAllSlides();

            return songs.Select(song => new SongDataVm
            {
                Id = song.Id,
                Name = song.Name,
                NameNormalized = song.NameNormalized,
                Indexable = song.Indexable,
                SlidesCount = songSlides.Count(x => x.SongId == song.Id),
                Created = song.Created,
                Modified = song.Modified,
                Protected = song.Protected,
                PaperSongBookId = song.PaperSongBookId
            }).ToArray();
        }

        [HttpPost]
        [Route("normalizeText")]
        public string NormalizeText(SearchVm searchVm)
        {
            return SearchHelper.NormalizeText(searchVm.TextToFind);
        }
    }
}
