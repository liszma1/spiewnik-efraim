const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ScriptExtPlugin = require('script-ext-html-webpack-plugin');
const { AngularCompilerPlugin } = require('@ngtools/webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const WebPack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { BaseHrefWebpackPlugin } = require('base-href-webpack-plugin');

module.exports = env => {
    const productionMode = !!env.production;
    const baseHref = '/Angular/';
    
    return {
        entry: ['./src/Core/Main.ts'],
        output: {
            path: __dirname + '/Deploy',
            filename: '[name].[chunkhash:8].js',
            chunkFilename: '[name][chunkhash].js',
        },
        resolve: {
            extensions: ['.ts', '.js']
        },
        module: {
            rules: [
                {
                    test: /\.ts$/, 
                    loader: '@ngtools/webpack'
                },
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: "html-loader",
                            options: { minimize: false }
                        }
                    ]
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    use: 'url-loader?limit=10000'
                },
                {
                    test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
                    use: 'file-loader'
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    use: [
                        'file-loader?name=images/[name].[ext]',
                        'image-webpack-loader?bypassOnDebug'
                    ]
                }
            ]
        },
        plugins: [
            new WebPack.HashedModuleIdsPlugin(),
            new CopyWebpackPlugin([{
                from: __dirname + '/web.config',
                to: __dirname + '/Deploy',
                force: true
            }]),
            new HtmlWebpackPlugin({
                template: __dirname + '/index.html',
                output: __dirname + '/Deploy',
                inject: 'head',
                chunksSortMode: 'none',
                favicon: 'src/favicon.ico'
            }),
            new BaseHrefWebpackPlugin({ baseHref: baseHref }),
            new ScriptExtPlugin({
                defaultAttribute: 'defer'
            }),
            new AngularCompilerPlugin({
                tsConfigPath: './tsconfig.json',
                entryModule: './src/Core/Module#SpiewnikEfraimMainModule',
                sourceMap: !productionMode
            }),
            productionMode ? new MiniCssExtractPlugin() : null,
            new WebPack.DefinePlugin({
                'PRODUCTION_MODE': productionMode,
                'BASE_HREF': baseHref
            }),
            new WebPack.ProvidePlugin({
                $: 'jquery', 
                jQuery: 'jquery'
            }),
            new CleanWebpackPlugin()
        ].filter(Boolean),
        mode: productionMode ? "production" : "development",
        devtool: productionMode ? "source-map" : "inline-source-map",
        optimization: {
            noEmitOnErrors: true,
            runtimeChunk: 'single',
            minimize: productionMode,
            minimizer: [new UglifyJsPlugin()],
            splitChunks: {
                cacheGroups: {
                    vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        chunks: 'all',
                        name: 'vendor_app',
                        minChunks: 2
                    },
                },                
                chunks: 'all'
            }
        },
        watch: !productionMode,
        watchOptions: {
            aggregateTimeout: 300,
            poll: 2000,
            ignored: /node_modules/
        },
        stats: {
            warningsFilter: /System.import/ //https://github.com/angular/angular/issues/21560
        }
    };
}