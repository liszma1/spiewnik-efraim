import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from './BaseService';
import { HttpClient } from '@angular/common/http';
import { ScreenDataVm } from '../ViewModels/ScreenDataVM';
import { StartScreenProcessVm } from '../ViewModels/StartScreenProcessVm';
import { SlideDataVm } from '../ViewModels/SlideDataVm';
import { TvActionDto } from '../Dto/TvActionDto';

@Injectable()
export class ScreenService extends BaseService {
    constructor(http: HttpClient) {
        super(http);
    }
    
    public handleServerCommand(serverCommand: string): Observable<void> {
        return this.post(serverCommand);
    }

    public restartServer(): Observable<void> {
        return this.post("restartServer");
    }

    public reconnectToExistingScreenProcess(): Observable<boolean> {
        return this.post("reconnectToExistingScreenProcess");
    }

    public getMonitorsInfo(): Observable<ScreenDataVm[]> {
        return this.post("getMonitorsInfo");
    }

    public startScreenProcess(refreshDisplayDriver: boolean): Observable<StartScreenProcessVm> {
        return this.post("startScreenProcess", { RefreshDisplayDriver: refreshDisplayDriver });
    }

    public getSlideContentFromExistingScreenProcess(): Observable<SlideDataVm> {
        return this.post("getSlideContentFromExistingScreenProcess");
    }

    public setSpecifiedContentOnScreen(slide: SlideDataVm): Observable<boolean> {
        var model = { ...slide };
      
        model.SearchPageText = "";

        if (!slide.AdHoc)
            model.PageText = "";

        return this.post("setSpecifiedContentOnScreen", model);
    }

    public setSpecifiedContentOnScreenBySongName(songName: string): Observable<number> {
        return this.post("setSpecifiedContentOnScreenBySongName", { SongName: songName });
    }

    public setPrimaryMonitor(): Observable<boolean> {
        return this.post("setPrimaryMonitor");
    }

    public setSecondaryMonitor(): Observable<boolean> {
        return this.post("setSecondaryMonitor");
    }
    
    public executeActionOnTv(action: TvActionDto): Observable<boolean> {
        return this.postWithQueryParams("executeActionOnTv", null, { actionToExecute: action });
    }
}