import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class FormBehavior {
    private errorSource = new Subject<string>();
    private waitBoxSource = new Subject<boolean>();
    private validationSource = new Subject<string[]>();
    private lockCommitSource = new Subject<boolean>();
    public errorObservable = this.errorSource.asObservable();
    public waitBoxObservable = this.waitBoxSource.asObservable();
    public validationObservable = this.validationSource.asObservable();
    public lockCommitObservable = this.lockCommitSource.asObservable();

    public HandleError(messageObject: any) {
        if (typeof messageObject !== "string")
            this.errorSource.next((messageObject.error.ExceptionMessage || messageObject.error.Message || messageObject.message));
        else
            this.errorSource.next(messageObject);
    }

    public SetWindowBusy(busy: boolean) {
        this.waitBoxSource.next(busy);
    }

    public HideAlertBox(alertBoxId: string) {
        $('#' + alertBoxId + '').hide();
    }
}