import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from './BaseService';
import { HttpClient } from '@angular/common/http';
import { SlideDataVm } from '../ViewModels/SlideDataVm';
import { SongDataVm } from '../ViewModels/SongDataVm';

@Injectable()
export class SearchService extends BaseService {
    constructor(http: HttpClient) {
        super(http);
    }
    
    public SearchForSlidesAndGetContent(textToFind: string, skipResultsFromSameSong: boolean): Observable<SlideDataVm[]> {
        return this.post("searchForSlidesAndGetContent",
        {
            TextToFind: textToFind,
            SkipResultsFromSameSong: skipResultsFromSameSong,
        });
    }

    public GetSlideData(slideId: number): Observable<SlideDataVm> {
        return this.post("getSlideData", { SlideId: slideId });
    }

    public GetSongDataBySongId(songId: number, skipHtml: boolean): Observable<SlideDataVm[]> {
        return this.post("getSongDataBySongId", { SongId: songId, SkipHtmlIfNotCustom: skipHtml });
    }

    public GetSpecialSongs(): Observable<SlideDataVm[]> {
        return this.getWithoutParams("getSpecialSongs");
    }

    public GetAllSongs(): Observable<SongDataVm[]> {
        return this.getWithoutParams("getAllSongs");
    }

    public NormalizeText(textToNormalize: string): Observable<string> {
        return this.post("normalizeText",  { TextToFind: textToNormalize });
    }
    
}