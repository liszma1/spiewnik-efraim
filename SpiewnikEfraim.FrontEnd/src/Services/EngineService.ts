import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BaseService } from './BaseService';
import { HttpClient } from '@angular/common/http';
import { SongFromEditorVm } from '../ViewModels/SongFromEditorVm';

@Injectable()
export class EngineService extends BaseService {
    constructor(http: HttpClient) {
        super(http);
    }
    
    public deleteSongFromDatabase(songId: number): Observable<boolean> {
        return this.post("deleteSongFromDatabase", { SongId: songId });
    }

    public saveSongToDatabase(song: SongFromEditorVm): Observable<number> {
        return this.post("saveSongToDatabase", song);
    }

    public RebuildDatabase(): Observable<boolean> {
        return this.post("rebuildDatabase", null);
    }

    public GetSlideTemplate(): Observable<string> {
        return this.getWithoutParams("getSlideTemplate");
    }

    public GetExportAllSongsAsHtmlUrl(): string {
        return this.getServiceUrl() + "exportSongsAsHtml";
    }

    public GetExportAllSongsAsJsonUrl(): string {
        return this.getServiceUrl() + "exportSongsAsJson";
    }

    public GetExportSongAsHtmlUrl(songId: number): string {
        return this.getServiceUrl() + "exportSongAsHtml?songId=" + songId;
    }

    public GetExportSongAsJsonUrl(songId: number): string {
        return this.getServiceUrl() + "exportSongAsJson?songId=" + songId;
    }    

    public ImportAllJsonFiles(files: FileList, forceSave: boolean): Observable<string[]> {
        let formData: FormData = new FormData();

        for(let i = 0; i < files.length; i++)
            formData.append(files.item(i).name, files.item(i));

        return this.postWithQueryParams("importSongsFromJson", formData, { forceSave: forceSave });
    }
}