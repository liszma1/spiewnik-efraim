import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBehavior } from "../Services/FormBehavior";
import { SlideChangeVm } from "../ViewModels/SlideChangeVm";
import { ScreenService } from "../Services/ScreenService";
import { finalize } from "rxjs/operators";
import { DataStorageHelper } from "../Helpers/DataStorageHelper";
import { SlideDataVm } from "../ViewModels/SlideDataVm";
import { PreviewComponent } from "../Controls/Preview";
import { AppSettings } from "../Dto/AppSettings";

@Component({
    selector: 'song-book',
    templateUrl: './SongBook.html'
})
export class SongBook implements OnInit {
    slideSent: boolean;
    textToFind: string;
    previewTabActive: boolean;
    isLoading: boolean = false;
    dataError: string;
    dataStorageHelper: DataStorageHelper;
    playlistItems: SlideDataVm[] = [];
    previousPageSize: string = "";
    settings: AppSettings;
    layoutObject: any = {
        CurrentSize: "xs"
    };
    @ViewChild("songBookPreviewComponent", {static: false}) songBookPreviewComponent: PreviewComponent;

    constructor(private formService: FormBehavior, private screenService: ScreenService) {        
    }

    ngOnInit() {
        this.dataStorageHelper = new DataStorageHelper();
        this.settings = this.dataStorageHelper.getSettings();
        this.adjustControlsSize();
        window.addEventListener('resize', () => {
            this.adjustControlsSize();
        }, true);

        this.formService.waitBoxObservable.subscribe(data => {
            this.isLoading = data;
        });

        this.formService.errorObservable.subscribe(data => {
            this.isLoading = false;
            this.dataError = data;
            $("#errorAlert").show();
        });
    }

    displayOnlyOneResultPerSongChanged(displayOneResultPerSong: boolean) {
        this.settings.displayOnlyOneResultPerSong = displayOneResultPerSong;
        this.dataStorageHelper.saveSettings(this.settings);
    }

    keyboardShortcutsEnabledChanged(enabled: boolean) {
        this.settings.keyboardShortcutsEnabled = enabled;
        this.dataStorageHelper.saveSettings(this.settings);
    }

    showAllSlidesUnderControlButtonsChanged(show: boolean) {
        this.settings.showAllSlidesUnderControlButtons = show;
        this.dataStorageHelper.saveSettings(this.settings);
    }

    onPillClick(pillName: string) {
        if (this.slideSent && pillName === "search")
            this.textToFind = "";

        if (pillName === "search") {
            setTimeout(() => {
                $("#textToFind").focus();
            }, 500);
        }

        this.previewTabActive = pillName === "preview";
        this.slideSent = false;
    }

    onSlideChange(event: SlideChangeVm) {
        if (event.SendSlideToTv) {
            if (event.Init) {
                if (this.songBookPreviewComponent)
                    this.songBookPreviewComponent.rebuildPreview(event.SlideData);
                    
                return;
            }

            ($('a[href$="preview"]') as any).tab("show");

            this.previewTabActive = true;
            this.slideSent = true;
            this.formService.SetWindowBusy(true);
            this.screenService.setSpecifiedContentOnScreen(event.SlideData).pipe(finalize(() => {
                this.formService.SetWindowBusy(false);
            })).subscribe(data => {
                if (!data) {
                    this.formService.HandleError("Zmiana slajdu nie powiodła się");
                    return;
                }

                if (this.songBookPreviewComponent)
                    this.songBookPreviewComponent.rebuildPreview(event.SlideData);

                //this.notifyAllClientsSlideChange(this.searchResultPageClicked);
            }, error => {
                this.formService.HandleError(error);
            });
        } else {
            if (this.songBookPreviewComponent)
                this.songBookPreviewComponent.addItemToPlaylist(event.SlideData);            
        }
    }

    hideAlertBox(alertBoxId: string) {
        this.dataError = "";
        this.formService.HideAlertBox(alertBoxId);
    }

    adjustControlsSize() {
        var currentSize = $('#users-device-size').find('div:visible').first().attr('id');

        if (this.previousPageSize === currentSize)
            return;

        this.layoutObject.CurrentSize = currentSize;
        this.previousPageSize = currentSize;
    }

    getContainerClass(): string {
        switch (this.previousPageSize) {
            case "xs":
            case "sm":
            case "md":
                return "container-fluid";
            default:
                return "container";
        }
    }

    showAdditionalTabs(): boolean { 
        switch (this.previousPageSize) {
            case "xs":
            case "sm":
            case "md":
                return false;
            default:
                return true;
        }
    }
}