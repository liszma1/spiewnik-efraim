import { Component, Output, EventEmitter, OnInit, Input, OnChanges, SimpleChanges, AfterViewInit } from "@angular/core";
import { FormBehavior } from "../Services/FormBehavior";
import { SearchService } from "../Services/SearchService";
import { finalize } from "rxjs/operators";
import { SlideDataVm } from "../ViewModels/SlideDataVm";
import { SlideChangeVm } from "../ViewModels/SlideChangeVm";
import { AndroidHelper } from "../Helpers/AndroidHelper";

@Component({
    selector: 'song-book-search',
    templateUrl: './Search.html'
})
export class SearchComponent implements OnInit, OnChanges, AfterViewInit {
    @Input() layoutSize: string;
    @Input() displayOnlyOneResultPerSong: boolean;
    @Output() onSlideChange = new EventEmitter<SlideChangeVm>();
    resultsFound: boolean = false;
    dataError: string = null;
    textToFind: string = "";
    foundData: SlideDataVm[] = [];
    currentResultsPageIndicator: number = 1;
    resultPagesCount: number = 1;
    searchResultSlide: SlideDataVm;
    androidHelper: AndroidHelper;
    results: SlideDataVm[];

    constructor(private formService: FormBehavior, private searchService: SearchService) {
    }

    ngOnChanges(change: SimpleChanges) {
        if (typeof change.layoutSize.currentValue !== "undefined" && change.layoutSize.currentValue !== change.layoutSize.previousValue)
            this.rebuildSearchResults();
    }

    ngOnInit() {
        this.androidHelper = new AndroidHelper();
    }

    ngAfterViewInit() {
        ($("#disableNotification") as any).modal("show");
    }

    goToPreviousSearchResults() {
        if (this.currentResultsPageIndicator === 1)
            return;

        this.currentResultsPageIndicator--;
        this.rebuildSearchResults();
    }

    goToNextSearchResults() {
        this.currentResultsPageIndicator++;
        this.rebuildSearchResults();
    }

    sendSlideToServer() {
        ($("#chooseActionDialog") as any).modal("hide");
        
        if (this.onSlideChange.observers.length > 0)
            this.onSlideChange.emit(new SlideChangeVm(true, this.searchResultSlide));
    }

    sendSlideToPlaylist() {
        ($("#chooseActionDialog") as any).modal("hide");
        
        if (this.onSlideChange.observers.length > 0)
            this.onSlideChange.emit(new SlideChangeVm(false, this.searchResultSlide));
    }

    onSlideClicked(slideData: SlideDataVm) {
        this.searchResultSlide = slideData;
    }

    hideAlertBox(alertBoxId: string) {
        this.formService.HideAlertBox(alertBoxId);
    }
    
    private clearError() {
        this.dataError = "";
        this.formService.HideAlertBox("errorAlert");
    }

    public searchSongs(event: any) {
        if (event)
            event.preventDefault();

        this.androidHelper.hideKeyboard($("#textToFind"));

        this.formService.SetWindowBusy(true);
        this.resultsFound = false;
        this.clearError();
        this.results = [];

        this.searchService.SearchForSlidesAndGetContent(this.textToFind, this.displayOnlyOneResultPerSong).pipe(finalize(() => {
            this.formService.SetWindowBusy(false);
        })).subscribe(data => {
            if (data !== null && typeof data !== "undefined" && data.length > 0)
                this.resultsFound = true;

            this.currentResultsPageIndicator = 1;
            this.foundData = data;
            this.rebuildSearchResults();

            if (!data || data.length == 0) {               
                $("#nothingFoundAlert").show();
                setTimeout(() => { 
                    $("#nothingFoundAlert").hide(); 
                }, 2000);
            }
        }, error => {
            this.formService.HandleError(error);
        });
    }

    private rebuildSearchResults() {
        if (this.foundData === null || this.foundData.length === 0)
            return;
        
        var maximumResults = 9;

        switch(this.layoutSize)
        {
            case "xs":
            case "sm":
            case "md":
                maximumResults = 6; break;
            default:
                break;
        }

        var pagesToDraw = null;
        this.resultPagesCount = Math.floor(this.foundData.length / maximumResults) + (this.foundData.length % maximumResults == 0 ? 0 : 1);

        if (this.foundData.length < maximumResults)
            pagesToDraw = this.foundData;
        else if (this.foundData.length >= this.currentResultsPageIndicator * maximumResults)
            pagesToDraw = this.foundData.slice((this.currentResultsPageIndicator - 1) * maximumResults, this.currentResultsPageIndicator * maximumResults);
        else
            pagesToDraw = this.foundData.slice((this.currentResultsPageIndicator - 1) * maximumResults);

        this.results = pagesToDraw;
    }

    getColumnCount(): number {
        switch(this.layoutSize) {
            case "xs":
                return 1;
            case "sm":
            case "md":
                return 2;
            default:
                return 3;
        }
    }
    
    getColumnClass(): string {
        switch(this.layoutSize) {
            case "xs":
                return "col-12";
            case "sm":
            case "md":
                return "col-6";
            default:
                return "col-4";
        }
    }
}