import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges, OnChanges } from "@angular/core";
import { FormBehavior } from "../Services/FormBehavior";
import { finalize } from 'rxjs/operators';
import { SlideDataVm } from "../ViewModels/SlideDataVm";
import { DataStorageHelper } from "../Helpers/DataStorageHelper";
import { SlideChangeVm } from "../ViewModels/SlideChangeVm";
import { SearchService } from "../Services/SearchService";
import { ScreenService } from "../Services/ScreenService";
import { LayoutInfo } from "../Dto/LayoutInfo";

@Component({
    selector: 'song-book-preview',
    templateUrl: './Preview.html'
})
export class PreviewComponent implements OnInit, OnChanges {
    @Input() layoutSize: string;
    @Input() keyboardShortcutsEnabled: boolean;
    @Input() previewTabActive: boolean;
    @Input() showAllSlidesUnderControlButtons: boolean;
    @Output() onSlideChange = new EventEmitter<SlideChangeVm>();
    layoutInfo: LayoutInfo = new LayoutInfo();
    blackoutTemplate: string;
    previewObject: any =
    {
        Previous: "",
        Current: "",
        Next: ""
    };
    dataStorageHelper: DataStorageHelper;
    currentSlide: SlideDataVm;
    songSlides: SlideDataVm[] = [];
    playlistItems: SlideDataVm[] = [];
    specialSlides: SlideDataVm[] = [];
    tripleColumnLayout: boolean = true;
    doubleColumnLayout: boolean = false;
    singleColumnLayout: boolean = false;

    constructor(private formService: FormBehavior, private searchService: SearchService, private screenService: ScreenService) {        
    }

    ngOnChanges(change: SimpleChanges) {
        if (typeof change.layoutSize !== "undefined" && typeof change.layoutSize.currentValue !== "undefined" && change.layoutSize.currentValue !== change.layoutSize.previousValue) {
            this.rebuildPreviewLayout();
        }
    }

    ngOnInit() {
        this.enableKeyboardShortcuts();
        this.dataStorageHelper = new DataStorageHelper();

        if (this.dataStorageHelper.playlistExistsInStorage())
            this.playlistItems = this.dataStorageHelper.getPlaylist();

        this.blackoutTemplate = "<html><body><div style=\"background-color: black; width: 100%; height: 100%;\"></div></body></html>";

        this.formService.SetWindowBusy(true);
        this.searchService.GetSpecialSongs().subscribe(data => {
           this.specialSlides = data; 
           this.reconnectToScreenProcess();
        }, error => {
            this.formService.HandleError(error);
            this.formService.SetWindowBusy(false);
        });       
    }

    reconnectToScreenProcess() {
        this.formService.SetWindowBusy(true);
        this.screenService.getSlideContentFromExistingScreenProcess().pipe(finalize(() => {
            this.formService.SetWindowBusy(false);
        })).subscribe(data => {
            if (!data) {
                this.formService.HandleError("Usługa wyświetlania slajdów na telewizorze nie jest dostępna. Spróbuj ponownie później");
            } else {
                if (data.SongId == -1)
                    this.setSpecialSong("Zaciemnienie", true);
                else
                    this.onSlideChange.emit(new SlideChangeVm(true, data, true)); 
            }
        }, error => {
            this.formService.HandleError(error);
        });
    }

    setSpecialSong(songName: string, initial: boolean) {
        for (let slide of this.specialSlides) {
            if (slide.SongName == songName) {
                this.onSlideChange.emit(new SlideChangeVm(true, slide, initial));
                break;
            }
        }
    }

    goToPreviousSlide() {
        if (this.currentSlide.Order == 1)
            return;

        if (this.onSlideChange.observers.length > 0)
            this.onSlideChange.emit(new SlideChangeVm(true, this.getPreviousSlide(this.currentSlide.Order)));
    }

    goToNextSlide() {
        if (this.currentSlide.Order == this.songSlides.length || this.songSlides.length == 0)
            return;

        if (this.onSlideChange.observers.length > 0)
            this.onSlideChange.emit(new SlideChangeVm(true, this.getNextSlide(this.currentSlide.Order)));
    }

    goToSlideBlackout() {
        if (this.currentSlide.SongName == "Zaciemnienie")
            return;

        for (let slide of this.specialSlides) {
            if (slide.SongName == "Zaciemnienie") {
                this.onSlideChange.emit(new SlideChangeVm(true, slide));
                break;
            }
        }
    }

    goToSlideWithLogo() {
        if (this.currentSlide.SongName == "Logo Efraim")
            return;

        for (let slide of this.specialSlides) {
            if (slide.SongName == "Logo Efraim") {
                this.onSlideChange.emit(new SlideChangeVm(true, slide));
                break;
            }
        }
    }

    goToSlideWithMary() {
        if (this.currentSlide.SongName == "Maryja")
            return;
    
        for (let slide of this.specialSlides) {
            if (slide.SongName == "Maryja") {
                this.onSlideChange.emit(new SlideChangeVm(true, slide));
                break;
            }
        }
    }

    selectItemFromPlaylist(slideData: SlideDataVm) {
        this.onSlideChange.emit(new SlideChangeVm(true, slideData));
    }

    selectItemFromSlides(slideData: SlideDataVm) {
        this.onSlideChange.emit(new SlideChangeVm(true, slideData));
    }

    removeItemFromPlaylist(index: number) {
        this.playlistItems.splice(index, 1);        
        this.dataStorageHelper.savePlaylist(this.playlistItems);
    }

    addItemToPlaylist(slideData: SlideDataVm) {
        this.playlistItems.push(slideData);
        this.dataStorageHelper.savePlaylist(this.playlistItems);
    }

    private setPreviewSlides(slideData: SlideDataVm) {   
        if (slideData.AdHoc) {
            this.previewObject.Previous = this.blackoutTemplate;
            this.previewObject.Current = slideData.PreviewHtml;
            this.previewObject.Next = this.blackoutTemplate;
        }
        else if (this.songSlides.length == 1 || slideData.SongId == 0) {
            this.previewObject.Previous = this.blackoutTemplate;
            this.previewObject.Current = slideData.PageText;
            this.previewObject.Next = this.blackoutTemplate;
        }
        else if (slideData.Order == 1) {
            this.previewObject.Previous = this.blackoutTemplate;
            this.previewObject.Current = slideData.PageText;
            this.previewObject.Next = this.getNextSlide(1).PageText;
        } else if (slideData.Order == this.songSlides.length) {
            this.previewObject.Previous = this.getPreviousSlide(slideData.Order).PageText;
            this.previewObject.Current = slideData.PageText;
            this.previewObject.Next = this.blackoutTemplate;
        } else {
            this.previewObject.Previous = this.getPreviousSlide(slideData.Order).PageText;
            this.previewObject.Current = slideData.PageText;
            this.previewObject.Next = this.getNextSlide(slideData.Order).PageText;
        }
    }

    rebuildPreview(slideData: SlideDataVm) {
        this.currentSlide = slideData;

        if (slideData.AdHoc || slideData.SongId == 0 || (this.songSlides && this.songSlides.length > 0 && this.songSlides[0].SongId == slideData.SongId)) {
            this.setPreviewSlides(slideData);
        } else {
            this.formService.SetWindowBusy(true);
            this.searchService.GetSongDataBySongId(this.currentSlide.SongId, false).pipe(finalize(() => {
                this.formService.SetWindowBusy(false);
            })).subscribe(data => {
                this.songSlides = data;
                this.setPreviewSlides(slideData);
            }, error => {
                this.formService.HandleError(error);
            });
        }  
    }

    private getNextSlide(currentSlideOrder: number): SlideDataVm {
        for(let i = 0; i < this.songSlides.length; i++) {
            if (currentSlideOrder + 1 == this.songSlides[i].Order)
                return this.songSlides[i];
        }

        return this.songSlides[this.songSlides.length - 1];
    }

    private getPreviousSlide(previousSlideOrder: number): SlideDataVm {
        for(let i = this.songSlides.length - 1; i > 0; i--) {
            if (this.songSlides[i].Order == previousSlideOrder - 1)
                return this.songSlides[i];
        }

        return this.songSlides[0];
    }

    private rebuildPreviewLayout() {
        switch(this.layoutSize) {
            case "xs":
            case "sm":
                this.layoutInfo.tripleColumnLayout = false;
                this.layoutInfo.doubleColumnLayout = false;
                this.layoutInfo.singleColumnLayout = true;

                this.tripleColumnLayout = false;
                this.doubleColumnLayout = false;
                this.singleColumnLayout = true;
                break;
            case "md":
                this.layoutInfo.tripleColumnLayout = false;
                this.layoutInfo.doubleColumnLayout = true;
                this.layoutInfo.singleColumnLayout = false;

                this.tripleColumnLayout = false;
                this.doubleColumnLayout = true;
                this.singleColumnLayout = false;
                break;
            default:
                this.layoutInfo.tripleColumnLayout = true;
                this.layoutInfo.doubleColumnLayout = false;
                this.layoutInfo.singleColumnLayout = false;

                this.tripleColumnLayout = true; 
                this.doubleColumnLayout = false;
                this.singleColumnLayout = false;
                break;
        }
    }

    hideAlertBox(alertBoxId: string) {
        this.formService.HideAlertBox(alertBoxId);
    }
    
    getLayoutColumnSize(): string {
        switch(this.layoutSize) {
            case "xs":
                return "col-12";
            case "sm":
            case "md":
                return "col-6";
            default:
                return "col-4";
        }
    }

    getColumnCount(): number {
        switch(this.layoutSize) {
            case "xs":
                return 1;
            case "sm":
            case "md":
                return 2;
            default:
                return 3;
        }
    }

    enableKeyboardShortcuts() {
        document.onkeyup = e => {
            if (!this.previewTabActive || !this.keyboardShortcutsEnabled)
                return;

            if (e.which === 37) // Arrow left
                this.goToPreviousSlide();
            else if (e.which === 39) // Arrow right
                this.goToNextSlide();
            else if (e.which === 36) // Home
                this.goToSlideWithLogo();
            else if (e.which === 35) // End
                this.goToSlideWithMary();
        };
    }
}