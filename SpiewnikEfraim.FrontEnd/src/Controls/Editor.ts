import { Component, Input, OnChanges, SimpleChanges, ViewChild, OnInit } from "@angular/core";
import { FormBehavior } from "../Services/FormBehavior";
import { SearchService } from "../Services/SearchService";
import { SongDataVm } from "../ViewModels/SongDataVm";
import { finalize } from "rxjs/operators";
import { SlideDataVm } from "../ViewModels/SlideDataVm";
import { EngineService } from "../Services/EngineService";
import { SongFromEditorVm } from "../ViewModels/SongFromEditorVm";
import { Table } from "primeng/table";
import { Subject } from "rxjs";

import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import "rxjs/add/operator/mergeMap";

@Component({
    selector: 'song-book-editor',
    templateUrl: './Editor.html'
})
export class EditorComponent implements OnChanges, OnInit {
    @Input() layoutSize: string;
    editorStarted: boolean = false;
    importerStarted: boolean = false;
    songEditing: boolean = false;
    slideEditing: boolean = false;
    songsList: SongDataVm[] = [];
    songSlides: SlideDataVm[] = [];
    currentSlide: SlideDataVm = new SlideDataVm();
    currentSlideIndex: number = -1;
    currentSong: SongDataVm = new SongDataVm();
    currentSongIndex: number = -1;
    currentEditedText: string = "";
    autoAlignLines: boolean = false;
    originalValue: string = "";
    newSongId: number = -1;
    songNameChanged = new Subject<string>();
    jsonfiles: FileList;
    jsonFilesSelected: boolean = false;
    forceSaveImport: boolean = false;
    importResults: string[] = [];
    @ViewChild("songsListTable", {static: false}) songsListTable: Table;

    constructor(private formService: FormBehavior, private searchService: SearchService, private engineService: EngineService) {
    }

    ngOnInit() {
        this.songNameChanged
            .debounceTime(350)
            .distinctUntilChanged()
            .subscribe(textToNormalize => {
                this.searchService.NormalizeText(textToNormalize).subscribe(data => {
                    this.songsListTable.filter(data, 'NameNormalized', 'contains');
                }, error => {
                    this.formService.HandleError(error);
                });
        });
    }

    ngOnChanges(change: SimpleChanges) {
        if (typeof change.layoutSize.currentValue !== "undefined" && change.layoutSize.currentValue !== change.layoutSize.previousValue) {
            // Zmiana layout'u
        }
    }

    exitImporter() {
        this.importerStarted = false;
    }

    exitEditor() {
        this.editorStarted = false;
    }

    startImporter() {
        this.importerStarted = true;
    }

    captureAllJsonFiles(files: FileList) {
        this.jsonfiles = files;
        this.jsonFilesSelected = true;
    }

    importAllJsonFiles() {
        this.importResults = [];
        this.formService.SetWindowBusy(true);
        this.engineService.ImportAllJsonFiles(this.jsonfiles, this.forceSaveImport).subscribe(data => {
            this.importResults = data;
            this.formService.SetWindowBusy(false);
        }, error => {
            this.formService.SetWindowBusy(false);
            this.formService.HandleError(error);
        });   
    }

    startEditor() {
        this.editorStarted = true;

        this.formService.SetWindowBusy(true);
        this.searchService.GetAllSongs().pipe(finalize(() => {
            this.formService.SetWindowBusy(false);
        })).subscribe(data => {
            this.songsList = data;
        }, error => {
            this.formService.HandleError(error);
        });
    }

    editSong(song: SongDataVm, index: number) {
        //if ((this.songEditing || this.slideEditing) && !confirm("Czy na pewno chcesz edytować inną pieśń? Prawdopodobnie istnieją niezapisane zmiany"))
        //    return;
        
        this.slideEditing = false;
        this.songEditing = false;
        this.currentSong = {...song};
        this.currentSongIndex = index;

        this.formService.SetWindowBusy(true);
        this.searchService.GetSongDataBySongId(song.Id, true).pipe(finalize(() => {
            this.formService.SetWindowBusy(false);
        })).subscribe(data => {
            this.songSlides = data;
            this.songEditing = true;
        }, error => {
            this.formService.HandleError(error);
        });
    }

    editSlide(slide: SlideDataVm, index: number) {
        this.currentSlideIndex = index;
        
        this.currentSlide = slide;
        this.originalValue = slide.PageText;
        this.slideEditing = true;
        this.autoAlignLines = false;

        if (slide.CustomHtml)
            this.currentEditedText = this.currentSlide.PageText;
        else
            this.currentEditedText = this.currentSlide.PageText.replace(/<br\/>/gi, "\n").replace(/<p class="slideParagraph">/gi, "").replace(/<\/p>/gi, "");
    }

    saveSlide() {
        this.currentSlide.Modified = new Date();
        this.currentSlideIndex = -1;
        this.slideEditing = false;
    }

    deleteSlide() {
        var slideOrder = this.currentSlide.Order;
        this.songSlides.splice(this.currentSlideIndex, 1);
        
        // Normalize slide order
        for(let slide of this.songSlides) {
            if (slide.Order >= slideOrder)
                slide.Order--;
        }

        this.currentSong.SlidesCount--;
        this.currentSlideIndex = -1;
        this.slideEditing = false;
    }

    cancelSlide() {
        this.currentSlide.PageText = this.originalValue;
        this.currentSlideIndex = -1;
        this.slideEditing = false;
    }

    autoAlignLinesChange() {
        if (this.currentSlide.CustomHtml)
            return;

        if (this.autoAlignLines)
            this.currentSlide.PageText = "<p class=\"slideParagraph\">" + this.currentEditedText.replace(/\n/gi, " ") + "</p>";
        else
            this.currentSlide.PageText = "<p class=\"slideParagraph\">" + this.currentEditedText.replace(/\n/gi, "<br/>") + "</p>";
    }

    currentEditedTextChange() {
        if (this.currentSlide.CustomHtml)
            this.currentSlide.PageText = this.currentEditedText;
        else {
            if (this.autoAlignLines)
                this.currentSlide.PageText = "<p class=\"slideParagraph\">" + this.currentEditedText.replace(/\n/gi, " ") + "</p>";
            else
                this.currentSlide.PageText = "<p class=\"slideParagraph\">" + this.currentEditedText.replace(/\n/gi, "<br/>") + "</p>";
        }
    }
    
    selectedSongCss(index: number): string {
        if (!this.currentSong)
            return null;

        if (index == this.currentSongIndex)
            return "selectedRowStyle";

        return null;
    }

    selectedSlideCss(index: number): string {
        if (!this.slideEditing || this.currentSlide.CustomHtml)
            return null;

        if (index == this.currentSlideIndex)
            return "selectedSlideStyle";

        return null;
    }

    addNewSong() {
        this.currentSong = new SongDataVm();
        this.currentSong.Id = this.newSongId;
        this.currentSong.Indexable = true;
        this.currentSong.SlidesCount = 0;
        this.currentSong.Name = "";
        this.currentSong.Created = new Date();
        this.currentSong.Modified = new Date();
        this.newSongId--;
          
        this.songSlides = [];
        this.currentSongIndex = -1;
        this.songEditing = true;
        this.slideEditing = false;
    }

    deleteSong(songData: SongDataVm) {
        if (songData.Protected) {
            this.formService.HandleError("Nie można usunąć systemowej pieśni");
            return;
        }

        if (!confirm("Czy na pewno chcesz usunąć wybraną pieśń? Operacja jest nieodwracalna"))
            return;

        if (songData.Id <= 0)
            return;

        this.formService.SetWindowBusy(true);
        this.engineService.deleteSongFromDatabase(songData.Id).subscribe(data => {
            if (data) {
                this.songEditing = false;
                this.slideEditing = false;
                this.currentSongIndex = -1;
                this.startEditor(); // Reload data
            } else {
                this.formService.SetWindowBusy(false);
                this.formService.HandleError("Wystąpił błąd podczas usuwania pieśni");
            }
        }, error => {
            this.formService.SetWindowBusy(false);
            this.formService.HandleError(error);
        });   
    }

    validate(): boolean {
        if (!this.songSlides || this.songSlides.length == 0) {
            this.formService.HandleError("Pieśń musi mieć co najmniej jeden slajd");
            return false;
        }

        if (!this.currentSong.Name || this.currentSong.Name == "") {
            this.formService.HandleError("Pieśń musi mieć nazwę");
            return false;
        }

        for(let song of this.songsList) {
            if (song.Name == this.currentSong.Name && song.Id != this.currentSong.Id) {
                this.formService.HandleError("Pieśń musi mieć unikalną nazwę");
                return false;
            }
        }

        return true;
    }

    saveSong() {
        if (!this.validate())
            return;

        this.currentSong.Modified = new Date();

        var model = new SongFromEditorVm();
        model.SongData = this.currentSong;
        model.SlidesData = this.songSlides;

        this.formService.SetWindowBusy(true);
        this.engineService.saveSongToDatabase(model).subscribe(data => {
            if (data < 0) {
                this.formService.HandleError("Wystąpił błąd podczas zapisu pieśni");
                this.formService.SetWindowBusy(false);
            }
            else {
                this.songEditing = false;
                
                //if (this.currentSong.Id <= 0) {
                    this.currentSongIndex = -1;
                    this.startEditor(); // Reload data
                /*} else {
                    this.songsList[this.currentSongIndex].Modified = this.currentSong.Modified;
                    this.songsList[this.currentSongIndex].Indexable = this.currentSong.Indexable;
                    this.songsList[this.currentSongIndex].Name = this.currentSong.Name;
                    this.songsList[this.currentSongIndex].SlidesCount = this.currentSong.SlidesCount;
                    this.currentSongIndex = -1;
                    this.formService.SetWindowBusy(false);
                }*/
            }
        }, error => {
            this.formService.HandleError(error);
            this.formService.SetWindowBusy(false);
        });
    }
    
    cancelSong() {
        this.songEditing = false;
        this.currentSongIndex = -1;
    }

    addSlide() {
        var newSlideData = new SlideDataVm();
        newSlideData.Order = this.songSlides.length + 1;
        newSlideData.CustomHtml = false;
        newSlideData.Id = 0;
        newSlideData.PageText = "";
        newSlideData.SearchPageText = "";
        newSlideData.Created = new Date();
        newSlideData.Modified = new Date();
        newSlideData.SongId = this.currentSong.Id;
        newSlideData.SongName = this.currentSong.Name;

        this.currentSong.SlidesCount++;
        this.songSlides.push(newSlideData);
        this.currentSlideIndex = this.songSlides.length - 1;
    }

    moveSlideUp() {
        if (this.currentSlide.Order == 1)
            return;

        let newSlideOrder = this.currentSlide.Order - 1;

        for(let slide of this.songSlides) {
            if (slide.Order == newSlideOrder)
                slide.Order = slide.Order + 1;
        }

        this.currentSlide.Order = newSlideOrder;
        this.currentSlideIndex--;

        this.songSlides.sort(this.sortSlides);
    }
    
    moveSlideDown() {
        if (this.currentSlide.Order == this.songSlides.length)
            return;

        let newSlideOrder = this.currentSlide.Order + 1;

        for(let slide of this.songSlides) {
            if (slide.Order == newSlideOrder)
                slide.Order = slide.Order - 1;
        }

        this.currentSlide.Order = newSlideOrder;
        this.currentSlideIndex++;

        this.songSlides.sort(this.sortSlides);
    }

    private sortSlides(a: SlideDataVm, b: SlideDataVm): number {
        if (a.Order < b.Order)
          return -1;

        if (a.Order > b.Order)
          return 1;

        return 0;
    }

    searchForName(value: string) {
        this.songNameChanged.next(value);
    }

    saveSongAsHtml() {
        this.forceDownload(this.engineService.GetExportSongAsHtmlUrl(this.currentSong.Id), "Song.zip");
    }

    saveSongAsJson() {
        this.forceDownload(this.engineService.GetExportSongAsJsonUrl(this.currentSong.Id), "Song.json");
    }

    saveSongsAsHtml() {
        this.forceDownload(this.engineService.GetExportAllSongsAsHtmlUrl(), "Songs.zip");
    }

    saveSongsAsJson() {
        this.forceDownload(this.engineService.GetExportAllSongsAsJsonUrl(), "Songs.json");
    }

    forceDownload(href: string, fileName: string) {
        var anchor = document.createElement('a');
        anchor.href = href;
        anchor.download = fileName;
        document.body.appendChild(anchor);
        anchor.click();
    }
}