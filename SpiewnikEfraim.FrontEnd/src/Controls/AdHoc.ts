import { Component, Output, EventEmitter, Input, OnChanges, SimpleChanges, OnInit } from "@angular/core";
import { FormBehavior } from "../Services/FormBehavior";
import { SlideChangeVm } from "../ViewModels/SlideChangeVm";
import { EngineService } from "../Services/EngineService";
import { finalize } from "rxjs/operators";
import { SlideDataVm } from "../ViewModels/SlideDataVm";

@Component({
    selector: 'song-book-adhoc',
    templateUrl: './AdHoc.html'
})
export class AdHocComponent implements OnChanges, OnInit {
    @Input() layoutSize: string;
    @Output() onSlideChange = new EventEmitter<SlideChangeVm>();
    textToShow: string = "";
    editAsHtml: boolean = false;
    autoAlignLines: boolean = false;
    canSendToTv: boolean = false;
    previewText: string = "";
    previewContent: string = "";
    previewTemplate: string = "";
    isLoading: boolean = true;

    constructor(private formService: FormBehavior, private engineService: EngineService) {
    }

    ngOnChanges(change: SimpleChanges) {
        if (typeof change.layoutSize.currentValue !== "undefined" && change.layoutSize.currentValue !== change.layoutSize.previousValue) {
            //this.rebuildSearchResults();
        }
    }

    ngOnInit() {
        this.engineService.GetSlideTemplate().pipe(finalize(() => {
            this.previewContent = this.previewTemplate.replace("{0}", "");
            this.isLoading = false;
        })).subscribe(data => {
            this.previewTemplate = data;
        }, error => {
            this.formService.HandleError(error);
        });
    }

    editAsHtmlChange() {
        this.canSendToTv = false;
        this.previewContent = this.previewTemplate.replace("{0}", "");
    }
    
    autoAlignLinesChange() {
        this.canSendToTv = false;
        this.previewContent = this.previewTemplate.replace("{0}", "");
    }

    textToShowChange() {
        this.canSendToTv = false;
        this.previewContent = this.previewTemplate.replace("{0}", "");
    }
    
    showPreview() {
        if (!this.editAsHtml) {
            if (this.autoAlignLines)
                this.previewText = "<p class=\"slideParagraph\">" + this.textToShow.replace(/\n/gi, " ") + "</p>";
            else
                this.previewText = "<p class=\"slideParagraph\">" + this.textToShow.replace(/\n/gi, "<br/>") + "</p>";
        
            this.previewContent = this.previewTemplate.replace("{0}", this.previewText);
        } else {
            this.previewContent = this.textToShow;
            this.previewText = this.textToShow;
        }

        this.canSendToTv = true;
    }
    
    sendToTv() {
        if (this.onSlideChange.observers.length == 0)
            return;

        var slideData = new SlideDataVm();
        slideData.AdHoc = true;
        slideData.Created = new Date();
        slideData.Modified = new Date();
        slideData.CustomHtml = this.editAsHtml;
        slideData.Order = 1;
        slideData.Id = 0;
        slideData.SearchPageText = "";
        slideData.SongId = 0;
        slideData.SongName = "";
        slideData.PageText = this.previewText;
        slideData.PreviewHtml = this.previewContent;
        
        this.onSlideChange.emit(new SlideChangeVm(true, slideData));
    }
}