﻿import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { SafeHtml } from "@angular/platform-browser"
import { DomSanitizer } from '@angular/platform-browser';
import { SearchService } from "../Services/SearchService";
import { FormBehavior } from "../Services/FormBehavior";
import { finalize } from "rxjs/operators";
import { LayoutInfo } from "../Dto/LayoutInfo";

@Component({
    selector: 'song-book-slide-preview',
    templateUrl: './SlidePreview.html'
})
export class SlidePreviewComponent implements OnChanges {
    @Input() slideContent: string;
    @Input() slideId: number;
    @Input() fullSize: boolean;
    @Input() additionalLayoutInfo: LayoutInfo;
    isLoading: boolean = true;
    slideContentHtml: SafeHtml;

    constructor(private domSanitizer: DomSanitizer, private searchService: SearchService, private formService: FormBehavior) {
    }    

    ngOnChanges(change: SimpleChanges) {        
        if (typeof change.slideContent !== "undefined" && 
            typeof change.slideContent.currentValue !== "undefined" && 
            change.slideContent.currentValue !== change.slideContent.previousValue) {           
            this.slideContentHtml = this.domSanitizer.bypassSecurityTrustHtml(this.slideContent);
            this.isLoading = false;
        }

        if (typeof change.slideId !== "undefined" && 
            typeof change.slideId.currentValue !== "undefined" && 
            change.slideId.currentValue != null && 
            change.slideId.currentValue !== 0 &&
            change.slideId.currentValue !== change.slideId.previousValue) {
            this.getSlideDataById();
        }
    }

    getSlideDataById() {
        this.isLoading = true;
        this.searchService.GetSlideData(this.slideId).pipe(finalize(() => {
            this.isLoading = false;
        })).subscribe(data => {
            this.slideContentHtml = this.domSanitizer.bypassSecurityTrustHtml(data.PageText);
        }, error => {
            this.formService.HandleError(error);
        });
    }

    getSlidePreviewClass(): string {
        if (!this.fullSize)
            return "playlistCanvas";
        
        if (!this.additionalLayoutInfo || this.additionalLayoutInfo.tripleColumnLayout || this.additionalLayoutInfo.doubleColumnLayout)
            return "previewCanvas";
        
        return "singlePreviewCanvas";
    }
}