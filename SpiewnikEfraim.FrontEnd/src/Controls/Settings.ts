import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ScreenDataVm } from "../ViewModels/ScreenDataVM";
import { FormBehavior } from "../Services/FormBehavior";
import { ScreenService } from "../Services/ScreenService";
import { finalize } from "rxjs/operators";
import { EngineService } from "../Services/EngineService";
import { AppSettings } from "../Dto/AppSettings";
import { TvActionDto } from "../Dto/TvActionDto";

@Component({
    selector: 'song-book-settings',
    templateUrl: './Settings.html'
})
export class SettingsComponent {
    @Input() layoutSize: string;
    @Input() settings: AppSettings;
    @Output() keyboardShortcutsEnabledChanged = new EventEmitter<boolean>();
    @Output() showAllSlidesUnderControlButtonsChanged = new EventEmitter<boolean>();
    @Output() displayOnlyOneResultPerSongChanged = new EventEmitter<boolean>();
    serverFunctionCaller: string;
    connectedToScreenService: boolean = false;
    monitorsInfo: ScreenDataVm[] = [];
    showAdvancedButton: boolean = true;
    advancedInfoMessage: string = "";
    
    constructor(private formService: FormBehavior, private screenService: ScreenService, private engineService: EngineService) {        
    }

    rebuildDatabase() {
        this.formService.SetWindowBusy(true);
        this.engineService.RebuildDatabase().pipe(finalize(() => {
            this.formService.SetWindowBusy(false);
        })).subscribe(data => {
            if (!data)
                this.formService.HandleError("Przebudowa bazy danych nie powiodła się. Przywróć ją z kopii zapasowej");
        }, error => {
            this.formService.HandleError(error);
        });
    }

    onServerCallerChange(caller: string) {
        this.serverFunctionCaller = caller;

        switch(this.serverFunctionCaller) {
            case "TurnOnTv":
                this.advancedInfoMessage = "włączyć telewizor"
                break;
            case "TurnOffTv":
                this.advancedInfoMessage = "wyłączyć telewizor";
                break;
            case "SwitchToHdmi1":
                this.advancedInfoMessage = "przełączyć na HDMI1";
                break;
            case "SwitchToHdmi2":
                this.advancedInfoMessage = "przełączyć na HDMI2";
                break;
            case "SwitchToHdmi3":
                this.advancedInfoMessage = "przełączyć na HDMI3";
                break;
            case "RebuildDatabase":
                this.advancedInfoMessage = "przebudować DB";
                break;
            case "Restart":
                this.advancedInfoMessage = "zrestartować serwer";
                break;
            case "Shutdown":
                this.advancedInfoMessage = "wyłączyć serwer";
                break;
        } 
    }

    setPrimaryMonitor() {
        this.formService.SetWindowBusy(true);
        this.screenService.setPrimaryMonitor().pipe(finalize(() => {
            this.formService.SetWindowBusy(false);
        })).subscribe(data => {
            if (!data)
                this.formService.HandleError("Usługa wyświetlania slajdów na telewizorze nie jest dostępna/zmiana monitora nie powiodła się");
        }, error => {
            this.formService.HandleError(error);
        });
    }

    setSecondaryMonitor() {
        this.formService.SetWindowBusy(true);
        this.screenService.setSecondaryMonitor().pipe(finalize(() => {
            this.formService.SetWindowBusy(false);
        })).subscribe(data => {
            if (!data)
                this.formService.HandleError("Usługa wyświetlania slajdów na telewizorze nie jest dostępna/zmiana monitora nie powiodła się");
        }, error => {
            this.formService.HandleError(error);
        });
    }

    reconnectToScreenProcess() {
        this.formService.SetWindowBusy(true);
        this.screenService.getSlideContentFromExistingScreenProcess().subscribe(data => {
            if (!data) {
                this.formService.HandleError("Usługa wyświetlania slajdów na telewizorze nie jest dostępna. Spróbuj ponownie później");
                this.formService.SetWindowBusy(false);
                return;
            } else
                this.connectedToScreenService = true;

            this.getMonitorsInfo();
        }, error => {
            this.formService.SetWindowBusy(false);
            this.formService.HandleError(error);
        });
    }

    getMonitorsInfo() {
        this.monitorsInfo = [];
        this.screenService.getMonitorsInfo().pipe(finalize(() => {
            this.formService.SetWindowBusy(false);
        })).subscribe(data => {
            if (data !== null)
                this.monitorsInfo = data;
        }, error => {
            this.formService.HandleError(error);
        });
    }

    onDisplayOnlyOneResultPerSongChanged() {
        if (this.displayOnlyOneResultPerSongChanged.observers.length > 0)
            this.displayOnlyOneResultPerSongChanged.emit(this.settings.displayOnlyOneResultPerSong);
    }

    onKeyboardShortcutsEnabledChanged() {
        if (this.keyboardShortcutsEnabledChanged.observers.length > 0)        
            this.keyboardShortcutsEnabledChanged.emit(this.settings.keyboardShortcutsEnabled);
    }

    onShowAllSlidesUnderControlButtonsChanged() {
        if (this.showAllSlidesUnderControlButtonsChanged.observers.length > 0)
            this.showAllSlidesUnderControlButtonsChanged.emit(this.settings.showAllSlidesUnderControlButtons);
    }

    handleVideoServer() {
        ($("#confirmDialog") as any).modal("hide");
        
        if (this.serverFunctionCaller === "RebuildDatabase") {
            this.rebuildDatabase();
            return;
        }

        var commandForTv: TvActionDto = null;

        switch(this.serverFunctionCaller) {
            case "TurnOnTv":
                commandForTv = TvActionDto.TurnOn;
                break;
            case "TurnOffTv":
                commandForTv = TvActionDto.TurnOff;
                break;
            case "SwitchToHdmi1":
                commandForTv = TvActionDto.SwitchToHdmi1;
                break;
            case "SwitchToHdmi2":
                commandForTv = TvActionDto.SwitchToHdmi2;
                break;
            case "SwitchToHdmi3":
                commandForTv = TvActionDto.SwitchToHdmi3;
                break;
        } 

        this.formService.SetWindowBusy(true);

        if (commandForTv != null) {
            this.screenService.executeActionOnTv(commandForTv).pipe(finalize(() => {
                this.formService.SetWindowBusy(false);
            })).subscribe(data => {
                if (!data)
                    this.formService.HandleError("Telewizor aktualnie przetwarza inne żądanie. Poczekaj chwilę i spróbuj ponownie");
            }, error => {
                this.formService.HandleError(error);
            });
        } else {
            this.screenService.handleServerCommand(this.serverFunctionCaller === "Restart" ? "restartServer" : "shutdownServer").pipe(finalize(() => {
                this.formService.SetWindowBusy(false);
            })).subscribe(() => {
                if (this.serverFunctionCaller === "Restart")
                    $("#restartServerAlert").show();
                else
                    $("#shutdownServerAlert").show();
            }, error => {
                this.formService.HandleError(error);
            });
        }
    }
}