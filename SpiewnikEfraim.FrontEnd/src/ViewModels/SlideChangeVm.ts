import { SlideDataVm } from "./SlideDataVm";

export class SlideChangeVm {
    SendSlideToTv: boolean;
    SlideData: SlideDataVm;
    Init: boolean;

    constructor(sendSlideToTv: boolean, slideData: SlideDataVm, initialSlide?: boolean) {
        this.SendSlideToTv = sendSlideToTv;
        this.SlideData = slideData;

        if (initialSlide)
            this.Init = initialSlide;
        else
            this.Init = false;
    }
}