export class SlideDataVm {
    Id: number;
    SongId: number;
    SongName: string;
    Order: number;
    PageText: string;
    SearchPageText: string;
    CustomHtml: boolean;
    Created: Date;
    Modified: Date;
    AdHoc: boolean;
    PreviewHtml: string;
}