import { RectangleDataVm } from "./RectangleDataVm";

export class ScreenDataVm {
    Primary: boolean;
    DeviceName: string;
    Bounds: RectangleDataVm;
    BitsPerPixel: number;
}