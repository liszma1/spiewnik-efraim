export enum StartScreenProcessVm {
    ScreenServiceNotAvailable = 0,
    NoDisplayAvailable = 1,
    ConfigLoadError = 2,
    PdfLoadError = 3,
    ResizeScreenError = 4,
    Ok = 5
}