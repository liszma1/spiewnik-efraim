import { SongDataVm } from "./SongDataVm";
import { SlideDataVm } from "./SlideDataVm";

export class SongFromEditorVm {
    SongData: SongDataVm;
    SlidesData: SlideDataVm[];
}