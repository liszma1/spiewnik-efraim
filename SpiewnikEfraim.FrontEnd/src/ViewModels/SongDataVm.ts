export class SongDataVm {
    Id: number;
    PaperSongBookId: string;
    Name: string;
    Indexable: boolean;
    SlidesCount: number;
    Created: Date;
    Modified: Date;
    Protected: boolean;
}