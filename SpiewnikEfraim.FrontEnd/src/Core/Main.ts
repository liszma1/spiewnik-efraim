import 'zone.js/dist/zone';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import '../Content/Site.css'
import 'primeicons/primeicons.css'
import 'primeng/resources/primeng.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import * as $ from "jquery";

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SpiewnikEfraimMainModule } from './Module';
import { enableProdMode } from '@angular/core';

declare var PRODUCTION_MODE: boolean;

if (PRODUCTION_MODE)
    enableProdMode();
    
platformBrowserDynamic().bootstrapModule(SpiewnikEfraimMainModule);