import { Component } from "@angular/core"

@Component({
    selector: "spiewnik-efraim-main-app",
    template: '<router-outlet></router-outlet>'
})

export class SpiewnikEfraimMainApp {
}