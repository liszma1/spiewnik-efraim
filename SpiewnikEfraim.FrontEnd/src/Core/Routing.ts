import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SongBook } from "../Forms/SongBook";

const appRoutes: Routes = [
    { path: '', component: SongBook }
    //{ path: '**', component: NotFoundComponent }
];

export const SpiewnikEfraimRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes/*, { enableTracing: true }*/);