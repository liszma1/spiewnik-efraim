import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { APP_BASE_HREF } from '@angular/common';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { SpiewnikEfraimMainApp } from './Component';

// Routing
import { SpiewnikEfraimRouting } from "./Routing";
import { PreviewComponent } from '../Controls/Preview';
import { SearchComponent } from '../Controls/Search';
import { SettingsComponent } from '../Controls/Settings';
import { SongBook } from '../Forms/SongBook';
import { FormBehavior } from '../Services/FormBehavior';
import { ScreenService } from '../Services/ScreenService';
import { SlidePreviewComponent } from '../Controls/SlidePreview';
import { SearchService } from '../Services/SearchService';
import { EditorComponent } from '../Controls/Editor';
import {TableModule} from 'primeng/table';
import { EngineService } from '../Services/EngineService';
import { AdHocComponent } from '../Controls/AdHoc';

export declare var BASE_HREF: string;

@NgModule({
    imports: [
        FormsModule,
        BrowserModule,
        SpiewnikEfraimRouting,
        HttpClientModule,
        NoopAnimationsModule,
        TableModule
    ],
    declarations: [
        SpiewnikEfraimMainApp,
        PreviewComponent,
        SearchComponent,
        SettingsComponent,
        SongBook,
        SlidePreviewComponent,
        EditorComponent,
        AdHocComponent
    ], 
    bootstrap: [SpiewnikEfraimMainApp],
    providers: [
        FormBehavior,
        ScreenService,
        SearchService,
        EngineService,
        {provide: APP_BASE_HREF, useValue : BASE_HREF}
    ]
})
export class SpiewnikEfraimMainModule {
}