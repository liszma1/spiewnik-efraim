export class AppSettings {
    constructor() {
        this.displayOnlyOneResultPerSong = true;
        this.keyboardShortcutsEnabled = true;
        this.showAllSlidesUnderControlButtons= false;
    }
    
    displayOnlyOneResultPerSong: boolean;
    keyboardShortcutsEnabled: boolean;
    showAllSlidesUnderControlButtons: boolean;
}