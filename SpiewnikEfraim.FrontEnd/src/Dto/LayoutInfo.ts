export class LayoutInfo {
    constructor() {
        this.tripleColumnLayout = false;
        this.doubleColumnLayout = false;
        this.singleColumnLayout= false;
    }

    tripleColumnLayout: boolean;
    doubleColumnLayout: boolean;
    singleColumnLayout: boolean;    
}