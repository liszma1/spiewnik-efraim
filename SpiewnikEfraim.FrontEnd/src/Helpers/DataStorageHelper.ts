import * as moment from 'moment';
import { SlideDataVm } from '../ViewModels/SlideDataVm';
import { Injectable } from '@angular/core';
import { AppSettings } from '../Dto/AppSettings';

export interface IDataStorageHelper {
    savePlaylist(playlistItems: SlideDataVm[]): void;
    getPlaylist(): SlideDataVm[];
    playlistExistsInStorage(): boolean;
}

export class DataStorageHelper implements IDataStorageHelper {
    constructor() {
        if (typeof (Storage) === "undefined") {
            alert("Przeglądarka nie obsługuje HTML5");
            return;
        }

        this.checkAndRemoveIfNecessary();
    }

    saveSettings(settings: AppSettings) {
        if (localStorage.getItem("efraimSettings") !== null)
            localStorage.removeItem("efraimSettings");

        localStorage.setItem("efraimSettings", JSON.stringify(settings));
    }

    getSettings(): AppSettings {
        if (localStorage.getItem("efraimSettings") === null)
            return new AppSettings();

        return JSON.parse(localStorage.getItem("efraimSettings"));
    }

    savePlaylist(playlistItems: SlideDataVm[]) {
        if (localStorage.getItem("efraimPlaylistTimestamp") !== null)
            localStorage.removeItem("efraimPlaylistTimestamp");

        if (localStorage.getItem("efraimPlaylist") !== null)
            localStorage.removeItem("efraimPlaylist");

        localStorage.setItem("efraimPlaylistTimestamp", moment().add(1, 'd').endOf('d').format());
        localStorage.setItem("efraimPlaylist", JSON.stringify(playlistItems));
    }

    getPlaylist(): SlideDataVm[] {
        if (localStorage.getItem("efraimPlaylist") === null)
            return null;

        return JSON.parse(localStorage.getItem("efraimPlaylist"));
    }

    playlistExistsInStorage(): boolean {
        return localStorage.getItem("efraimPlaylist") !== null;
    }

    private checkAndRemoveIfNecessary() {
        if (localStorage.getItem("efraimPlaylist") === null && localStorage.getItem("efraimPlaylistTimestamp") !== null)
            localStorage.removeItem("efraimPlaylistTimestamp");

        if (localStorage.getItem("efraimPlaylist") !== null && localStorage.getItem("efraimPlaylistTimestamp") === null)
            localStorage.removeItem("efraimPlaylist");

        var timestampItem = localStorage.getItem("efraimPlaylistTimestamp");

        if (timestampItem !== null && moment() > moment(timestampItem)) {
            localStorage.removeItem("efraimPlaylist");
            localStorage.removeItem("efraimPlaylistTimestamp");
        }
    }
}