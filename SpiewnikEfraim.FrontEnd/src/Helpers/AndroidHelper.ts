export interface IAndroidHelper {
    hideKeyboard(element: any): void;
}

export class AndroidHelper implements IAndroidHelper {
    hideKeyboard(textElement: any) {
        textElement.attr('readonly', 'readonly'); // Force keyboard to hide on input field.
        textElement.attr('disabled', 'true'); // Force keyboard to hide on textarea field.
        setTimeout(function () {
            textElement.blur();  //actually close the keyboard
            // Remove readonly attribute after keyboard is hidden.
            textElement.removeAttr('readonly');
            textElement.removeAttr('disabled');
        }, 100);
    }
    
}