﻿using System.Net;
using SpiewnikEfraim.Logic.Services;

namespace SpiewnikEfraim.PdfParser
{
    public class ParserConfigProvider : IConfigProvider
    {
        public ParserConfigProvider(string databasePath)
        {
            DatabasePath = databasePath;
        }

        public string DatabasePath { get; }
        public int TvPort => 0;
        public bool IntegrationDisabled => true;
        public string SlideTemplate => null;
        public IPAddress TvAddress => null;
    }
}
