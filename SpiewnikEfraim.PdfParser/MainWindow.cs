﻿using System;
using System.Windows.Forms;
using SpiewnikEfraim.Logic.Helpers;

namespace SpiewnikEfraim.PdfParser
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {            
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            //Visible = false;

            
        }

        private void HandleError()
        {
            MessageBox.Show("Generowanie pliku konfiguracyjnego zostało przerwane");
            Application.Exit();
        }

        private void ConfigFileDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var pdfResult = PdfFileDialog.ShowDialog();

            if (pdfResult != DialogResult.OK)
                HandleError();

            var saveResult = ConfigFileDialog.ShowDialog();

            if (saveResult != DialogResult.OK)
                HandleError();

            var parserHelper = new ParserHelper(PdfFileDialog.FileName);
            parserHelper.WriteParsedTextToDatabase(ConfigFileDialog.FileName);

            MessageBox.Show("Gotowe!");
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var databaseFileResult = databaseFileDialog.ShowDialog();

            if (databaseFileResult != DialogResult.OK)
                HandleError();

            var dbHelper = new DbHelper(new ParserConfigProvider(databaseFileDialog.FileName));

            if (!dbHelper.RebuildDatabase())
                throw new Exception("Przebudowanie bazy danych nie powiodło się");
            
            MessageBox.Show("Gotowe!");
            Application.Exit();
        }
    }
}
